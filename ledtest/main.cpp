#include "../rxlibrary/common/rxlibrary.hpp"

namespace {
	// ベースクロックの定義(ベースクロック12MHz, 内部クロック96Mhz, 外部クロック出力なし)
	typedef device::system_io<12'000'000, false> SYSTEM_IO;
	// LED接続ポートの定義(PORT15に接続)
	typedef device::PORT<device::PORT1, device::bitpos::B5> LED;
}

int main(int argc, char** argv);

int main(int argc, char** argv) {
	SYSTEM_IO::setup_system_clock();

	LED::OUTPUT();

	while(1) {
		LED::P = 1;
		for(int i = 0; i < 48000000; i++) {
			asm("nop");
		}
		LED::P = 0;
		for(int i = 0; i < 48000000; i++) {
			asm("nop");
		}
	}
}