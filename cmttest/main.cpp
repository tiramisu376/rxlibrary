#include "../rxlibrary/common/rxlibrary.hpp"
#include "../rxlibrary/common/cmt_io.hpp"

namespace {
	// ベースクロックの定義(ベースクロック12MHz, 内部クロック96Mhz, 外部クロック出力なし)
	typedef device::system_io<12'000'000, false> SYSTEM_IO;
	// LED接続ポートの定義(PORT15に接続)
	typedef device::PORT<device::PORT1, device::bitpos::B5> LED;

	typedef device::cmt_io<device::CMT0, utils::null_task> CMT;
	CMT timer0;
}

int main(int argc, char** argv);
INTERRUPT_FUNC void timer_func(void);

uint8_t led = 1;
int main(int argc, char** argv) {
	SYSTEM_IO::setup_system_clock();

	LED::OUTPUT();
	LED::P = 1;

	bool ret = timer0.startMs(500, 4, timer_func);

	//bool ret = false;

	if (ret == false) {
		while(1) {
			LED::P = 1;
			for(int i = 0; i < 12000000; i++) {
				asm("nop");
			}
			LED::P = 0;
			for(int i = 0; i < 12000000; i++) {
				asm("nop");
			}
		}
	}

	uint32_t cnt = 0;
	while(1) {
		asm("nop");
	}
}


INTERRUPT_FUNC void timer_func(void) {
	LED::P = 0;
	led = led ^ 1;
}