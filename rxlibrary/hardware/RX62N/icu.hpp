//=====================================================//
// RX62Nグループ　ICUa定義
//=====================================================//
#pragma once

#include "icu_utils.hpp"

#if !defined(SIG_RX62N)
#error "icu.hpp requires SIG_RX62N to be defined"
#endif

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 割り込みコントローラ(ICUa)
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class _>
	struct icu_t {
		//*****************************************************//
		// ベクターインデックス
		//*****************************************************//
		enum class VECTOR : uint8_t {
			NONE	 = 0,	// ベクター0

			BUSERR	 = 16,	// バスエラー

			FIFERR	 = 21,	// フラッシュインタフェースエラー

			FRDYI	 = 23,	// フラッシュレディ

			SWINT	 = 27,	// ソフトウェア起動
			CMI0	 = 28,	// CMT0コンペアマッチ
			CMI1	 = 29,	// CMT1コンペアマッチ
			CMI2	 = 30,	// CMT2コンペアマッチ
			CMI3	 = 31,	// CMT3コンペアマッチ

			EINT	 = 32,	// EDMAC割り込み要求

			D0FIFO0	 = 36,	// USB0 D0FIFO転送要求
			D1FIFO0	 = 37,	// USB0 D1FIFO転送要求
			USBI0	 = 38,	// USB0割り込み

			D0FIFO1	 = 40,	// USB1 D0FIFO転送要求
			D1FIFO1	 = 41,	// USB1 D0FIFO転送要求
			USBI1	 = 42,	// USB1割り込み

			SPEI0	 = 44,	// RSPI0エラー
			SPRI0	 = 45,	// RSPI0受信
			SPTI0	 = 46,	// RSPI0送信
			SPII0	 = 47,	// RSPI0アイドル
			SPEI1	 = 48,	// RSPI1エラー
			SPRI1	 = 49,	// RSPI1受信
			SPTI1	 = 50,	// RSPI1送信
			SPII1	 = 51,	// RSPI1アイドル

			ERS0	 = 56,	// CAN0エラー
			RXF0	 = 57,	// CAN0受信FIFO
			TXF0	 = 58,	// CAN0送信FIFO
			RXM0	 = 59,	// CAN0受信完了
			TXM0	 = 60,	// CAN0送信完了

			PRD		 = 62,	// RTC周期
			CUP		 = 63,	// RTC桁上げ

			IRQ0	 = 64,	// IRQ0
			IRQ1	 = 65,	// IRQ1
			IRQ2	 = 66,	// IRQ2
			IRQ3	 = 67,	// IRQ3
			IRQ4	 = 68,	// IRQ4
			IRQ5	 = 69,	// IRQ5
			IRQ6	 = 70,	// IRQ6
			IRQ7	 = 71,	// IRQ7
			IRQ8	 = 72,	// IRQ8
			IRQ9	 = 73,	// IRQ9
			IRQ10	 = 74,	// IRQ10
			IRQ11	 = 75,	// IRQ11
			IRQ12	 = 76,	// IRQ12
			IRQ13	 = 77,	// IRQ13
			IRQ14	 = 78,	// IRQ14
			IRQ15	 = 79,	// IRQ15

			USBR0	 = 90,	// USB0割り込み
			USBR1	 = 91,	// USB1割り込み

			ALM		 = 92,	// RTCアラーム

			WOVI	 = 96,	// ウォッチドックタイマ

			ADI0	 = 98,	// AD0 A/D変換終了
			ADI1	 = 99,	// AD1 A/D変換終了

			S12ADI0  = 102,	// S12AD A/Dスキャン変換終了

			TGIA0	 = 114,	// MTU0.TGRAインプットキャプチャ/コンペアマッチ
			TGIB0	 = 115,	// MTU0.TGRBインプットキャプチャ/コンペアマッチ
			TGIC0	 = 116,	// MTU0.TGRCインプットキャプチャ/コンペアマッチ
			TGID0	 = 117,	// MTU0.TGRDインプットキャプチャ/コンペアマッチ
			TCIV0	 = 118,	// MTU0.TCNTオーバーフロー
			TGIE0	 = 119,	// MTU0.TGREインプットキャプチャ/コンペアマッチ
			TGIF0	 = 120,	// MTU0.TGRFインプットキャプチャ/コンペアマッチ
			TGIA1	 = 121,	// MTU1.TGRAインプットキャプチャ/コンペアマッチ
			TGIB1	 = 122,	// MTU1.TGRBインプットキャプチャ/コンペアマッチ
			TCIV1	 = 123,	// MTU1.TCNTオーバーフロー
			TCIU1	 = 124,	// MTU1.TCNTアンダーフロー
			TGIA2	 = 125,	// MTU2.TGRAインプットキャプチャ/コンペアマッチ
			TGIB2	 = 126,	// MTU2.TGRBインプットキャプチャ/コンペアマッチ
			TCIV2	 = 127,	// MTU2.TCNTオーバーフロー
			TCIU2	 = 128,	// MTU2.TCNTアンダーフロー
			TGIA3	 = 129,	// MTU3.TGRAインプットキャプチャ/コンペアマッチ
			TGIB3	 = 130,	// MTU3.TGRBインプットキャプチャ/コンペアマッチ
			TGIC3	 = 131,	// MTU3.TGRCインプットキャプチャ/コンペアマッチ
			TGID3	 = 132,	// MTU3.TGRDインプットキャプチャ/コンペアマッチ
			TCIV3	 = 133,	// MTU3.TCNTオーバーフロー
			TGIA4	 = 134,	// MTU4.TGRAインプットキャプチャ/コンペアマッチ
			TGIB4	 = 135,	// MTU4.TGRBインプットキャプチャ/コンペアマッチ
			TGIC4	 = 136,	// MTU4.TGRCインプットキャプチャ/コンペアマッチ
			TGID4	 = 137,	// MTU4.TGRDインプットキャプチャ/コンペアマッチ
			TCIV4	 = 138,	// MTU4.TCNTオーバーフロー/アンダーフロー
			TGIU5	 = 139,	// MTU5.TGRUインプットキャプチャ/コンペアマッチ
			TGIV5	 = 140,	// MTU5.TGRVインプットキャプチャ/コンペアマッチ
			TGIW5	 = 141,	// MTU5.TGRWインプットキャプチャ/コンペアマッチ
			TGIA6	 = 142,	// MTU6.TGRAインプットキャプチャ/コンペアマッチ
			TGIB6	 = 143,	// MTU6.TGRBインプットキャプチャ/コンペアマッチ
			TGIC6	 = 144,	// MTU6.TGRCインプットキャプチャ/コンペアマッチ
			TGID6	 = 145,	// MTU6.TGRDインプットキャプチャ/コンペアマッチ
			TCIV6	 = 146,	// MTU6.TCNTオーバーフロー
			TGIE6	 = 147,	// MTU6.TGREコンペアマッチ
			TGIF6	 = 148,	// MTU6.TGRFコンペアマッチ
			TGIA7	 = 149,	// MTU7.TGRAインプットキャプチャ/コンペアマッチ
			TGIB7	 = 150,	// MTU7.TGRBインプットキャプチャ/コンペアマッチ
			TCIV7	 = 151,	// MTU7.TCNTオーバーフロー
			TCIU7	 = 152,	// MTU7.TCNTアンダーフロー
			TGIA8	 = 153,	// MTU8.TGRAインプットキャプチャ/コンペアマッチ
			TGIB8	 = 154,	// MTU8.TGRBインプットキャプチャ/コンペアマッチ
			TCIV8	 = 155,	// MTU8.TCNTオーバーフロー
			TCIU8	 = 156,	// MTU8.TCNTアンダーフロー
			TGIA9	 = 157,	// MTU9.TGRAインプットキャプチャ/コンペアマッチ
			TGIB9	 = 158,	// MTU9.TGRBインプットキャプチャ/コンペアマッチ
			TGIC9	 = 159,	// MTU9.TGRCインプットキャプチャ/コンペアマッチ
			TGID9	 = 160,	// MTU9.TGRDインプットキャプチャ/コンペアマッチ
			TCIV9	 = 161,	// MTU9.TCNTオーバーフロー
			TGIA10	 = 162,	// MTU10.TGRAインプットキャプチャ/コンペアマッチ
			TGIB10	 = 163,	// MTU10.TGRBインプットキャプチャ/コンペアマッチ
			TGIC10	 = 164,	// MTU10.TGRCインプットキャプチャ/コンペアマッチ
			TGID10	 = 165,	// MTU10.TGRDインプットキャプチャ/コンペアマッチ
			TCIV10	 = 166,	// MTU10.TCNTオーバーフロー/アンダーフロー
			TGIU11	 = 167,	// MTU11.TGRUインプットキャプチャ/コンペアマッチ
			TGIV11	 = 168,	// MTU11.TGRVインプットキャプチャ/コンペアマッチ
			TGIW11	 = 169,	// MTU11.TGRWインプットキャプチャ/コンペアマッチ

			OEI1	 = 170,	// POEアウトプットイネーブル1
			OEI2	 = 171,	// POEアウトプットイネーブル2
			OEI3	 = 172,	// POEアウトプットイネーブル3
			OEI4	 = 173,	// POEアウトプットイネーブル4

			CMIA0	 = 174,	// TMR0.TCORAコンペアマッチ
			CMIB0	 = 175,	// TMR0.TCORBコンペアマッチ
			OVI0	 = 176,	// TMR0.TCNTオーバーフロー
			CMIA1	 = 177,	// TMR1.TCORAコンペアマッチ
			CMIB1	 = 178,	// TMR1.TCORBコンペアマッチ
			OVI1	 = 179,	// TMR1.TCNTオーバーフロー
			CMIA2	 = 180,	// TMR2.TCORAコンペアマッチ
			CMIB2	 = 181,	// TMR2.TCORBコンペアマッチ
			OVI2	 = 182,	// TMR2.TCNTオーバーフロー
			CMIA3	 = 183,	// TMR3.TCORAコンペアマッチ
			CMIB3	 = 184,	// TMR3.TCORBコンペアマッチ
			OVI3	 = 185,	// TMR3.TCNTオーバーフロー

			DMACI0	 = 198,	// DMACA
			DMACI1	 = 199,	// DMACA
			DMACI2	 = 200,	// DMACA
			DMACI3	 = 201,	// DMACA

			EXDMACI0 = 202,	// EXDMAC
			EXDMACI1 = 203,	// EXDMAC

			ERI0	 = 214,	// SCI0受信エラー
			RXI0	 = 215,	// SCI0受信データフル
			TXI0	 = 216,	// SCI0送信データエンプティ
			TEI0	 = 217,	// SCI0送信終了
			ERI1	 = 218,	// SCI1受信エラー
			RXI1	 = 219,	// SCI1受信データフル
			TXI1	 = 220,	// SCI1送信データエンプティ
			TEI1	 = 221,	// SCI1送信エラー
			ERI2	 = 222,	// SCI2受信エラー
			RXI2	 = 223,	// SCI2受信データフル
			TXI2	 = 224,	// SCI2送信データエンプティ
			TEI2	 = 225,	// SCI2送信終了
			ERI3	 = 226,	// SCI3受信エラー
			RXI3	 = 227,	// SCI3受信データフル
			TXI3	 = 228,	// SCI3送信データエンプティ
			TEI3	 = 229,	// SCI3送信終了

			ERI5	 = 234,	// SCI5受信エラー
			RXI5	 = 235,	// SCI5受信データフル
			TXI5	 = 236,	// SCI5送信データエンプティ
			TEI5	 = 237,	// SCI5送信終了
			ERI6	 = 238,	// SCI6受信エラー
			RXI6	 = 239,	// SCI6受信データフル
			TXI6	 = 240,	// SCI6送信データエンプティ
			TEI6	 = 241,	// SCI6送信終了

			ICEEI0	 = 246,	// RIIC0通信エラー/イベント発生
			ICRXI0	 = 247,	// RIIC0受信データフル
			ICTXI0	 = 248,	// RIIC0送信データエンプティ
			ICTEI0	 = 249,	// RIIC0送信終了
			ICEEI1	 = 250,	// RIIC1通信エラー/イベント発生
			ICRXI1	 = 251,	// RIIC1受信データフル
			ICTXI1	 = 252,	// RIIC1送信データエンプティ
			ICTEI1	 = 253,	// RIIC1送信終了
		};


		//*****************************************************//
		// IRレジスタ
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct ir_t {
			rw8_t<base + 16>  BUSERR;

			rw8_t<base + 21>  FIFERR;

			rw8_t<base + 23>  FRDYI;

			rw8_t<base + 27>  SWINT;

			rw8_t<base + 28>  CMI0;
			rw8_t<base + 29>  CMI1;
			rw8_t<base + 30>  CMI2;
			rw8_t<base + 31>  CMI3;

			rw8_t<base + 32>  EINT;

			rw8_t<base + 36>  D0FIFO0;
			rw8_t<base + 37>  D1FIFO0;
			rw8_t<base + 38>  USBI0;

			rw8_t<base + 40>  D0FIFO1;
			rw8_t<base + 41>  D1FIFO1;
			rw8_t<base + 42>  USBI1;

			rw8_t<base + 44>  SPEI0;
			rw8_t<base + 45>  SPRI0;
			rw8_t<base + 46>  SPTI0;
			rw8_t<base + 47>  SPII0;

			rw8_t<base + 48>  SPEI1;
			rw8_t<base + 49>  SPRI1;
			rw8_t<base + 50>  SPTI1;
			rw8_t<base + 51>  SPII1;

			rw8_t<base + 56>  ERS0;
			rw8_t<base + 57>  RXF0;
			rw8_t<base + 58>  TXF0;
			rw8_t<base + 59>  RXM0;
			rw8_t<base + 60>  TXM0;

			rw8_t<base + 62>  PRD;
			rw8_t<base + 63>  CUP;

			rw8_t<base + 64>  IRQ0;
			rw8_t<base + 65>  IRQ1;
			rw8_t<base + 66>  IRQ2;
			rw8_t<base + 67>  IRQ3;
			rw8_t<base + 68>  IRQ4;
			rw8_t<base + 69>  IRQ5;
			rw8_t<base + 70>  IRQ6;
			rw8_t<base + 71>  IRQ7;
			rw8_t<base + 72>  IRQ8;
			rw8_t<base + 73>  IRQ9;
			rw8_t<base + 74>  IRQ10;
			rw8_t<base + 75>  IRQ11;
			rw8_t<base + 76>  IRQ12;
			rw8_t<base + 77>  IRQ13;
			rw8_t<base + 78>  IRQ14;
			rw8_t<base + 79>  IRQ15;

			rw8_t<base + 90>  USBR0;
			rw8_t<base + 91>  USBR1;

			rw8_t<base + 92>  ALM;

			rw8_t<base + 96>  WOVI;

			rw8_t<base + 98>  ADI0;
			rw8_t<base + 99>  ADI1;

			rw8_t<base + 102> S12ADI0;

			rw8_t<base + 114> TGIA0;
			rw8_t<base + 115> TGIB0;
			rw8_t<base + 116> TGIC0;
			rw8_t<base + 117> TGID0;
			rw8_t<base + 118> TCIV0;
			rw8_t<base + 119> TGIE0;
			rw8_t<base + 120> TGIF0;

			rw8_t<base + 121> TGIA1;
			rw8_t<base + 122> TGIB1;
			rw8_t<base + 123> TCIV1;
			rw8_t<base + 124> TCIU1;

			rw8_t<base + 125> TGIA2;
			rw8_t<base + 126> TGIB2;
			rw8_t<base + 127> TCIV2;
			rw8_t<base + 128> TCIU2;

			rw8_t<base + 129> TGIA3;
			rw8_t<base + 130> TGIB3;
			rw8_t<base + 131> TGIC3;
			rw8_t<base + 132> TGID3;
			rw8_t<base + 133> TCIV3;

			rw8_t<base + 134> TGIA4;
			rw8_t<base + 135> TGIB4;
			rw8_t<base + 136> TGIC4;
			rw8_t<base + 137> TGID4;
			rw8_t<base + 138> TCIV4;

			rw8_t<base + 139> TGIU5;
			rw8_t<base + 140> TGIV5;
			rw8_t<base + 141> TGIW5;

			rw8_t<base + 142> TGIA6;
			rw8_t<base + 143> TGIB6;
			rw8_t<base + 144> TGIC6;
			rw8_t<base + 145> TGID6;
			rw8_t<base + 146> TCIV6;
			rw8_t<base + 147> TGIE6;
			rw8_t<base + 148> TGIF6;

			rw8_t<base + 149> TGIA7;
			rw8_t<base + 150> TGIB7;
			rw8_t<base + 151> TCIV7;
			rw8_t<base + 152> TCIU7;

			rw8_t<base + 153> TGIA8;
			rw8_t<base + 154> TGIB8;
			rw8_t<base + 155> TCIV8;
			rw8_t<base + 156> TCIU8;

			rw8_t<base + 157> TGIA9;
			rw8_t<base + 158> TGIB9;
			rw8_t<base + 159> TGIC9;
			rw8_t<base + 160> TGID9;
			rw8_t<base + 161> TCIV9;

			rw8_t<base + 162> TGIA10;
			rw8_t<base + 163> TGIB10;
			rw8_t<base + 164> TGIC10;
			rw8_t<base + 165> TGID10;
			rw8_t<base + 166> TCIV10;

			rw8_t<base + 167> TGIU11;
			rw8_t<base + 168> TGIV11;
			rw8_t<base + 169> TGIW11;

			rw8_t<base + 170> OEI1;
			rw8_t<base + 171> OEI2;
			rw8_t<base + 172> OEI3;
			rw8_t<base + 173> OEI4;

			rw8_t<base + 174> CMIA0;
			rw8_t<base + 175> CMIB0;
			rw8_t<base + 176> OVI0;

			rw8_t<base + 177> CMIA1;
			rw8_t<base + 178> CMIB1;
			rw8_t<base + 179> OVI1;

			rw8_t<base + 180> CMIA2;
			rw8_t<base + 181> CMIB2;
			rw8_t<base + 182> OVI2;

			rw8_t<base + 183> CMIA3;
			rw8_t<base + 184> CMIB3;
			rw8_t<base + 185> OVI3;

			rw8_t<base + 198> DMACI0;
			rw8_t<base + 199> DMACI1;
			rw8_t<base + 200> DMACI2;
			rw8_t<base + 201> DMACI3;

			rw8_t<base + 202> EXDMACI0;
			rw8_t<base + 203> EXDMACI1;

			rw8_t<base + 214> ERI0;
			rw8_t<base + 215> RXI0;
			rw8_t<base + 216> TXI0;
			rw8_t<base + 217> TEI0;

			rw8_t<base + 218> ERI1;
			rw8_t<base + 219> RXI1;
			rw8_t<base + 220> TXI1;
			rw8_t<base + 221> TEI1;

			rw8_t<base + 222> ERI2;
			rw8_t<base + 223> RXI2;
			rw8_t<base + 224> TXI2;
			rw8_t<base + 225> TEI2;

			rw8_t<base + 226> ERI3;
			rw8_t<base + 227> RXI3;
			rw8_t<base + 228> TXI3;
			rw8_t<base + 229> TEI3;

			rw8_t<base + 234> ERI5;
			rw8_t<base + 235> RXI5;
			rw8_t<base + 236> TXI5;
			rw8_t<base + 237> TEI5;

			rw8_t<base + 238> ERI6;
			rw8_t<base + 239> RXI6;
			rw8_t<base + 240> TXI6;
			rw8_t<base + 241> TEI6;

			rw8_t<base + 246> ICEEI0;
			rw8_t<base + 247> ICRXI0;
			rw8_t<base + 248> ICTXI0;
			rw8_t<base + 249> ICTEI0;

			rw8_t<base + 250> ICEEI1;
			rw8_t<base + 251> ICRXI1;
			rw8_t<base + 252> ICTXI1;
			rw8_t<base + 253> ICTEI1;

			//*****************************************************//
			// []オペレータ
			// param[in]	idx : インデックス(0～255)
			// return		IRレジスターの参照
			//*****************************************************//
			volatile uint8_t& operator [] (uint8_t idx) {
				return *reinterpret_cast<volatile uint8_t*>(base + idx);
			}
		};
		typedef ir_t<0x00087010> IR_;
		static IR_ IR;


		//*****************************************************//
		// IERレジスタ
		//*****************************************************//
		template <uint32_t base>
		struct ier_t {
			typedef rw8_t<base + 0x02> ier02;
			bit_rw_t<ier02, bitpos::B0>  BUSERR;
			bit_rw_t<ier02, bitpos::B5>  FIFERR;
			bit_rw_t<ier02, bitpos::B7>  FRDYI;

			typedef rw8_t<base + 0x03> ier03;
			bit_rw_t<ier03, bitpos::B3> SWINT;
			bit_rw_t<ier03, bitpos::B4> CMI0;
			bit_rw_t<ier03, bitpos::B5> CMI1;
			bit_rw_t<ier03, bitpos::B6> CMI2;
			bit_rw_t<ier03, bitpos::B7> CMI3;

			typedef rw8_t<base + 0x04> ier04;
			bit_rw_t<ier04, bitpos::B0> EINT;
			bit_rw_t<ier04, bitpos::B4> D0FIFO0;
			bit_rw_t<ier04, bitpos::B5> D1FIFO0;
			bit_rw_t<ier04, bitpos::B6> USBI0;

			typedef rw8_t<base + 0x05> ier05;
			bit_rw_t<ier05, bitpos::B0> D0FIFO1;
			bit_rw_t<ier05, bitpos::B1> D1FIFO1;
			bit_rw_t<ier05, bitpos::B2> USBI1;
			bit_rw_t<ier05, bitpos::B4> SPEI0;
			bit_rw_t<ier05, bitpos::B5> SPRI0;
			bit_rw_t<ier05, bitpos::B6> SPTI0;
			bit_rw_t<ier05, bitpos::B7> SPII0;

			typedef rw8_t<base + 0x06> ier06;
			bit_rw_t<ier06, bitpos::B0> SPEI1;
			bit_rw_t<ier06, bitpos::B1> SPRI1;
			bit_rw_t<ier06, bitpos::B2> SPTI1;
			bit_rw_t<ier06, bitpos::B3> SPII1;

			typedef rw8_t<base + 0x07> ier07;
			bit_rw_t<ier07, bitpos::B0> ERS0;
			bit_rw_t<ier07, bitpos::B1> RXF0;
			bit_rw_t<ier07, bitpos::B2> TXF0;
			bit_rw_t<ier07, bitpos::B3> RXM0;
			bit_rw_t<ier07, bitpos::B4> TXM0;
			bit_rw_t<ier07, bitpos::B6> PRD;
			bit_rw_t<ier07, bitpos::B7> CUP;

			typedef rw8_t<base + 0x08> ier08;
			bit_rw_t<ier08, bitpos::B0> IRQ0;
			bit_rw_t<ier08, bitpos::B1> IRQ1;
			bit_rw_t<ier08, bitpos::B2> IRQ2;
			bit_rw_t<ier08, bitpos::B3> IRQ3;
			bit_rw_t<ier08, bitpos::B4> IRQ4;
			bit_rw_t<ier08, bitpos::B5> IRQ5;
			bit_rw_t<ier08, bitpos::B6> IRQ6;
			bit_rw_t<ier08, bitpos::B7> IRQ7;

			typedef rw8_t<base + 0x09> ier09;
			bit_rw_t<ier09, bitpos::B0> IRQ8;
			bit_rw_t<ier09, bitpos::B1> IRQ9;
			bit_rw_t<ier09, bitpos::B2> IRQ10;
			bit_rw_t<ier09, bitpos::B3> IRQ11;
			bit_rw_t<ier09, bitpos::B4> IRQ12;
			bit_rw_t<ier09, bitpos::B5> IRQ13;
			bit_rw_t<ier09, bitpos::B6> IRQ14;
			bit_rw_t<ier09, bitpos::B7> IRQ15;

			typedef rw8_t<base + 0x0b> ier0b;
			bit_rw_t<ier0b, bitpos::B2> USBR0;
			bit_rw_t<ier0b, bitpos::B3> USBR1;
			bit_rw_t<ier0b, bitpos::B4> ALM;

			typedef rw8_t<base + 0x0c> ier0c;
			bit_rw_t<ier0c, bitpos::B0> WOVI;
			bit_rw_t<ier0c, bitpos::B2> ADI0;
			bit_rw_t<ier0c, bitpos::B3> ADI1;
			bit_rw_t<ier0c, bitpos::B6> S12ADI0;

			typedef rw8_t<base + 0x0e> ier0e;
			bit_rw_t<ier0e, bitpos::B2> TGIA0;
			bit_rw_t<ier0e, bitpos::B3> TGIB0;
			bit_rw_t<ier0e, bitpos::B4> TGIC0;
			bit_rw_t<ier0e, bitpos::B5> TGID0;
			bit_rw_t<ier0e, bitpos::B6> TCIV0;
			bit_rw_t<ier0e, bitpos::B7> TGIE0;

			typedef rw8_t<base + 0x0f> ier0f;
			bit_rw_t<ier0f, bitpos::B0> TGIF0;
			bit_rw_t<ier0f, bitpos::B1> TGIA1;
			bit_rw_t<ier0f, bitpos::B2> TGIB1;
			bit_rw_t<ier0f, bitpos::B3> TCIV1;
			bit_rw_t<ier0f, bitpos::B4> TCIU1;
			bit_rw_t<ier0f, bitpos::B5> TGIA2;
			bit_rw_t<ier0f, bitpos::B6> TGIB2;
			bit_rw_t<ier0f, bitpos::B7> TCIV2;

			typedef rw8_t<base + 0x10> ier10;
			bit_rw_t<ier10, bitpos::B0> TCIU2;
			bit_rw_t<ier10, bitpos::B1> TGIA3;
			bit_rw_t<ier10, bitpos::B2> TGIB3;
			bit_rw_t<ier10, bitpos::B3> TGIC3;
			bit_rw_t<ier10, bitpos::B4> TGID3;
			bit_rw_t<ier10, bitpos::B5> TCIV3;
			bit_rw_t<ier10, bitpos::B6> TGIA4;
			bit_rw_t<ier10, bitpos::B7> TGIB4;

			typedef rw8_t<base + 0x11> ier11;
			bit_rw_t<ier11, bitpos::B0> TGIC4;
			bit_rw_t<ier11, bitpos::B1> TGID4;
			bit_rw_t<ier11, bitpos::B2> TCIV4;
			bit_rw_t<ier11, bitpos::B3> TGIU5;
			bit_rw_t<ier11, bitpos::B4> TGIV5;
			bit_rw_t<ier11, bitpos::B5> TGIW5;
			bit_rw_t<ier11, bitpos::B6> TGIA6;
			bit_rw_t<ier11, bitpos::B7> TGIB6;

			typedef rw8_t<base + 0x12> ier12;
			bit_rw_t<ier12, bitpos::B0> TGIC6;
			bit_rw_t<ier12, bitpos::B1> TGID6;
			bit_rw_t<ier12, bitpos::B2> TCIV6;
			bit_rw_t<ier12, bitpos::B3> TGIE6;
			bit_rw_t<ier12, bitpos::B4> TGIF6;
			bit_rw_t<ier12, bitpos::B5> TGIA7;
			bit_rw_t<ier12, bitpos::B6> TGIB7;
			bit_rw_t<ier12, bitpos::B7> TCIV7;

			typedef rw8_t<base + 0x13> ier13;
			bit_rw_t<ier13, bitpos::B0> TCIU7;
			bit_rw_t<ier13, bitpos::B1> TGIA8;
			bit_rw_t<ier13, bitpos::B2> TGIB8;
			bit_rw_t<ier13, bitpos::B3> TCIV8;
			bit_rw_t<ier13, bitpos::B4> TCIU8;
			bit_rw_t<ier13, bitpos::B5> TGIA9;
			bit_rw_t<ier13, bitpos::B6> TGIB9;
			bit_rw_t<ier13, bitpos::B7> TGIC9;

			typedef rw8_t<base + 0x14> ier14;
			bit_rw_t<ier14, bitpos::B0> TGID9;
			bit_rw_t<ier14, bitpos::B1> TCIV9;
			bit_rw_t<ier14, bitpos::B2> TGIA10;
			bit_rw_t<ier14, bitpos::B3> TGIB10;
			bit_rw_t<ier14, bitpos::B4> TGIC10;
			bit_rw_t<ier14, bitpos::B5> TGID10;
			bit_rw_t<ier14, bitpos::B6> TCIV10;
			bit_rw_t<ier14, bitpos::B7> TGIU11;

			typedef rw8_t<base + 0x15> ier15;
			bit_rw_t<ier15, bitpos::B0> TGIV11;
			bit_rw_t<ier15, bitpos::B1> TGIW11;
			bit_rw_t<ier15, bitpos::B2> OEI1;
			bit_rw_t<ier15, bitpos::B3> OEI2;
			bit_rw_t<ier15, bitpos::B4> OEI3;
			bit_rw_t<ier15, bitpos::B5> OEI4;
			bit_rw_t<ier15, bitpos::B6> CMIA0;
			bit_rw_t<ier15, bitpos::B7> CMIB0;

			typedef rw8_t<base + 0x16> ier16;
			bit_rw_t<ier16, bitpos::B0> OVI0;
			bit_rw_t<ier16, bitpos::B1> CMIA1;
			bit_rw_t<ier16, bitpos::B2> CMIB1;
			bit_rw_t<ier16, bitpos::B3> OVI1;
			bit_rw_t<ier16, bitpos::B4> CMIA2;
			bit_rw_t<ier16, bitpos::B5> CMIB2;
			bit_rw_t<ier16, bitpos::B6> OVI2;
			bit_rw_t<ier16, bitpos::B7> CMIA3;

			typedef rw8_t<base + 0x17> ier17;
			bit_rw_t<ier17, bitpos::B0> CMIB3;
			bit_rw_t<ier17, bitpos::B1> OVI3;

			typedef rw8_t<base + 0x18> ier18;
			bit_rw_t<ier18, bitpos::B6> DMACI0;
			bit_rw_t<ier18, bitpos::B7> DMACI1;

			typedef rw8_t<base + 0x19> ier19;
			bit_rw_t<ier19, bitpos::B0> DMACI2;
			bit_rw_t<ier19, bitpos::B1> DMACI3;
			bit_rw_t<ier19, bitpos::B2> EXDMACI0;
			bit_rw_t<ier19, bitpos::B3> EXDMACI1;

			typedef rw8_t<base + 0x1a> ier1a;
			bit_rw_t<ier1a, bitpos::B6> ERI0;
			bit_rw_t<ier1a, bitpos::B7> RXI0;

			typedef rw8_t<base + 0x1b> ier1b;
			bit_rw_t<ier1b, bitpos::B0> TXI0;
			bit_rw_t<ier1b, bitpos::B1> TEI0;
			bit_rw_t<ier1b, bitpos::B2> ERI1;
			bit_rw_t<ier1b, bitpos::B3> RXI1;
			bit_rw_t<ier1b, bitpos::B4> TXI1;
			bit_rw_t<ier1b, bitpos::B5> TEI1;
			bit_rw_t<ier1b, bitpos::B6> ERI2;
			bit_rw_t<ier1b, bitpos::B7> RXI2;

			typedef rw8_t<base + 0x1c> ier1c;
			bit_rw_t<ier1c, bitpos::B0> TXI2;
			bit_rw_t<ier1c, bitpos::B1> TEI2;
			bit_rw_t<ier1c, bitpos::B2> ERI3;
			bit_rw_t<ier1c, bitpos::B3> RXI3;
			bit_rw_t<ier1c, bitpos::B4> TXI3;
			bit_rw_t<ier1c, bitpos::B5> TEI3;

			typedef rw8_t<base + 0x1d> ier1d;
			bit_rw_t<ier1d, bitpos::B2> ERI5;
			bit_rw_t<ier1d, bitpos::B3> RXI5;
			bit_rw_t<ier1d, bitpos::B4> TXI5;
			bit_rw_t<ier1d, bitpos::B5> TEI5;
			bit_rw_t<ier1d, bitpos::B6> ERI6;
			bit_rw_t<ier1d, bitpos::B7> RXI6;

			typedef rw8_t<base + 0x1e> ier1e;
			bit_rw_t<ier1e, bitpos::B0> TXI6;
			bit_rw_t<ier1e, bitpos::B1> TEI6;
			bit_rw_t<ier1e, bitpos::B6> ICEEI0;
			bit_rw_t<ier1e, bitpos::B7> ICRXI0;

			typedef rw8_t<base + 0x1f> ier1f;
			bit_rw_t<ier1f, bitpos::B0> ICTXI0;
			bit_rw_t<ier1f, bitpos::B1> ICTEI0;
			bit_rw_t<ier1f, bitpos::B2> ICEEI1;
			bit_rw_t<ier1f, bitpos::B3> ICRXI1;
			bit_rw_t<ier1f, bitpos::B4> ICTXI1;
			bit_rw_t<ier1f, bitpos::B5> ICTEI1;

			//*****************************************************//
			// 許可、不許可
			// param[in]	idx : インデックス(0～255)
			// param[in]	ena : 許可/不許可
			// return		なし
			//*****************************************************//
			void enable(uint8_t idx, bool ena) noexcept {
				auto tmp = rd8_(base + (idx>> 3));
				if (ena) {
					tmp |=   1 << (idx & 7);
				} else {
					tmp &= ~(1 << (idx & 7));
				}
				wr8_(base + (idx >> 3), tmp);
			}

			//*****************************************************//
			// 許可状態を取得
			// param[in]	idx : インデックス(0～255)
			// return		許可状態(許可の場合「true」)
			//*****************************************************//
			bool get(uint8_t idx) const noexcept {
				auto tmp = rd8_(base + (idx >> 3));
				return tmp & (1 << (idx & 7));
			}
		};
		typedef ier_t<0x00087200> IER_;
		static IER_ IER;


		//*****************************************************//
		// IPRレジスタ
		// 全て、下位4ビットが有効
		//*****************************************************//
		template <uint32_t base>
		struct ipr_t {
			rw8_t<base + 0>	  BUSERR;

			rw8_t<base + 1>   FIFERR;

			rw8_t<base + 2>   FRDYI;

			rw8_t<base + 3>   SWINT;

			rw8_t<base + 4>  CMI0;
			rw8_t<base + 5>  CMI1;
			rw8_t<base + 6>  CMI2;
			rw8_t<base + 7>  CMI3;

			rw8_t<base + 8>  EINT;

			rw8_t<base + 12>  D0FIFO0;
			rw8_t<base + 13>  D1FIFO0;
			rw8_t<base + 14>  USBI0;

			rw8_t<base + 16>  D0FIFO1;
			rw8_t<base + 17>  D1FIFO1;
			rw8_t<base + 18>  USBI1;

			rw8_t<base + 20>  SPEI0;
			rw8_t<base + 20>  SPRI0;
			rw8_t<base + 20>  SPTI0;
			rw8_t<base + 20>  SPII0;

			rw8_t<base + 21>  SPEI1;
			rw8_t<base + 21>  SPRI1;
			rw8_t<base + 21>  SPTI1;
			rw8_t<base + 21>  SPII1;

			rw8_t<base + 24>  ERS0;
			rw8_t<base + 24>  RXF0;
			rw8_t<base + 24>  TXF0;
			rw8_t<base + 24>  RXM0;
			rw8_t<base + 24>  TXM0;

			rw8_t<base + 30>  PRD;
			rw8_t<base + 31>  CUP;

			rw8_t<base + 32>  IRQ0;
			rw8_t<base + 33>  IRQ1;
			rw8_t<base + 34>  IRQ2;
			rw8_t<base + 35>  IRQ3;
			rw8_t<base + 36>  IRQ4;
			rw8_t<base + 37>  IRQ5;
			rw8_t<base + 38>  IRQ6;
			rw8_t<base + 39>  IRQ7;
			rw8_t<base + 40>  IRQ8;
			rw8_t<base + 41>  IRQ9;
			rw8_t<base + 42>  IRQ10;
			rw8_t<base + 43>  IRQ11;
			rw8_t<base + 44>  IRQ12;
			rw8_t<base + 45>  IRQ13;
			rw8_t<base + 46>  IRQ14;
			rw8_t<base + 47>  IRQ15;

			rw8_t<base + 58>  USBR0;
			rw8_t<base + 59>  USBR1;

			rw8_t<base + 60>  ALM;

			rw8_t<base + 64>  WOVI;

			rw8_t<base + 68>  ADI0;
			rw8_t<base + 69>  ADI1;

			rw8_t<base + 72>  S12ADI0;

			rw8_t<base + 81>  TGIA0;
			rw8_t<base + 81>  TGIB0;
			rw8_t<base + 81>  TGIC0;
			rw8_t<base + 81>  TGID0;
			rw8_t<base + 82>  TCIV0;
			rw8_t<base + 82>  TGIE0;
			rw8_t<base + 82>  TGIF0;

			rw8_t<base + 83>  TGIA1;
			rw8_t<base + 83>  TGIB1;
			rw8_t<base + 84>  TCIV1;
			rw8_t<base + 84>  TCIU1;

			rw8_t<base + 85>  TGIA2;
			rw8_t<base + 85>  TGIB2;
			rw8_t<base + 86>  TCIV2;
			rw8_t<base + 86>  TCIU2;

			rw8_t<base + 87>  TGIA3;
			rw8_t<base + 87>  TGIB3;
			rw8_t<base + 87>  TGIC3;
			rw8_t<base + 87>  TGID3;
			rw8_t<base + 88>  TCIV3;

			rw8_t<base + 89>  TGIA4;
			rw8_t<base + 89>  TGIB4;
			rw8_t<base + 89>  TGIC4;
			rw8_t<base + 89>  TGID4;
			rw8_t<base + 90>  TCIV4;

			rw8_t<base + 91>  TGIU5;
			rw8_t<base + 91>  TGIV5;
			rw8_t<base + 91>  TGIW5;

			rw8_t<base + 92>  TGIA6;
			rw8_t<base + 92>  TGIB6;
			rw8_t<base + 92>  TGIC6;
			rw8_t<base + 92>  TGID6;
			rw8_t<base + 93>  TCIV6;
			rw8_t<base + 93>  TGIE6;
			rw8_t<base + 93>  TGIF6;

			rw8_t<base + 94>  TGIA7;
			rw8_t<base + 94>  TGIB7;
			rw8_t<base + 95>  TCIV7;
			rw8_t<base + 95>  TCIU7;

			rw8_t<base + 96>  TGIA8;
			rw8_t<base + 96>  TGIB8;
			rw8_t<base + 97>  TCIV8;
			rw8_t<base + 97>  TCIU8;

			rw8_t<base + 98>  TGIA9;
			rw8_t<base + 98>  TGIB9;
			rw8_t<base + 98>  TGIC9;
			rw8_t<base + 98>  TGID9;
			rw8_t<base + 99>  TCIV9;

			rw8_t<base + 100> TGIA10;
			rw8_t<base + 100> TGIB10;
			rw8_t<base + 100> TGIC10;
			rw8_t<base + 100> TGID10;
			rw8_t<base + 101> TCIV10;

			rw8_t<base + 102> TGIU11;
			rw8_t<base + 102> TGIV11;
			rw8_t<base + 103> TGIW11;

			rw8_t<base + 103> OEI1;
			rw8_t<base + 103> OEI2;
			rw8_t<base + 103> OEI3;
			rw8_t<base + 103> OEI4;

			rw8_t<base + 104> CMIA0;
			rw8_t<base + 104> CMIB0;
			rw8_t<base + 104> OVI0;

			rw8_t<base + 105> CMIA1;
			rw8_t<base + 105> CMIB1;
			rw8_t<base + 105> OVI1;

			rw8_t<base + 106> CMIA2;
			rw8_t<base + 106> CMIB2;
			rw8_t<base + 106> OVI2;

			rw8_t<base + 107> CMIA3;
			rw8_t<base + 107> CMIB3;
			rw8_t<base + 107> OVI3;

			rw8_t<base + 112> DMACI0;
			rw8_t<base + 113> DMACI1;
			rw8_t<base + 114> DMACI2;
			rw8_t<base + 115> DMACI3;

			rw8_t<base + 116> EXDMACI0;
			rw8_t<base + 117> EXDMACI1;

			rw8_t<base + 128> ERI0;
			rw8_t<base + 128> RXI0;
			rw8_t<base + 128> TXI0;
			rw8_t<base + 128> TEI0;

			rw8_t<base + 129> ERI1;
			rw8_t<base + 129> RXI1;
			rw8_t<base + 129> TXI1;
			rw8_t<base + 129> TEI1;

			rw8_t<base + 130> ERI2;
			rw8_t<base + 130> RXI2;
			rw8_t<base + 130> TXI2;
			rw8_t<base + 130> TEI2;

			rw8_t<base + 131> ERI3;
			rw8_t<base + 131> RXI3;
			rw8_t<base + 131> TXI3;
			rw8_t<base + 131> TEI3;

			rw8_t<base + 133> ERI5;
			rw8_t<base + 133> RXI5;
			rw8_t<base + 133> TXI5;
			rw8_t<base + 133> TEI5;

			rw8_t<base + 134> ERI6;
			rw8_t<base + 134> RXI6;
			rw8_t<base + 134> TXI6;
			rw8_t<base + 134> TEI6;

			rw8_t<base + 136> ICEEI0;
			rw8_t<base + 137> ICRXI0;
			rw8_t<base + 138> ICTXI0;
			rw8_t<base + 139> ICTEI0;

			rw8_t<base + 140> ICEEI1;
			rw8_t<base + 141> ICRXI1;
			rw8_t<base + 142> ICTXI1;
			rw8_t<base + 143> ICTEI1;

			//*****************************************************//
			// []オペレータ
			// param[in]	idx : インデックス(0～255)
			// return		IPRレジスターの参照
			//*****************************************************//
			volatile uint8_t& operator [] (uint8_t idx) {
				return *reinterpret_cast<volatile uint8_t*>(base + idx);
			}
		};
		typedef ipr_t<0x00087300> IPR_;
		static IPR_ IPR;


		//*****************************************************//
		// DMACA起動要因レジスタn(DMRSRn)(n = DMACAチャネル番号)
		//*****************************************************//
		typedef rw8_t<0x00087400> DMRSR0_;
		static DMRSR0_ DMRSR0;

		typedef rw8_t<0x00087404> DMRSR1_;
		static DMRSR1_ DMRSR1;

		typedef rw8_t<0x00087408> DMRSR2_;
		static DMRSR2_ DMRSR2;

		typedef rw8_t<0x0008740c> DMRSR3_;
		static DMRSR3_ DMRSR3;


		//*****************************************************//
		// IRQコントロールレジスタn(IRQCRn)(n = IRQ番号)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct irqcr_t : public rw8_t<base> {
			typedef rw8_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bits_rw_t<io_, bitpos::B2, 2> IRQMD;
		};
		static irqcr_t<0x00087500> IRQCR0;
		static irqcr_t<0x00087501> IRQCR1;
		static irqcr_t<0x00087502> IRQCR2;
		static irqcr_t<0x00087503> IRQCR3;
		static irqcr_t<0x00087504> IRQCR4;
		static irqcr_t<0x00087505> IRQCR5;
		static irqcr_t<0x00087506> IRQCR6;
		static irqcr_t<0x00087507> IRQCR7;
		static irqcr_t<0x00087508> IRQCR8;
		static irqcr_t<0x00087509> IRQCR9;
		static irqcr_t<0x0008750a> IRQCR10;
		static irqcr_t<0x0008750b> IRQCR11;
		static irqcr_t<0x0008750c> IRQCR12;
		static irqcr_t<0x0008750d> IRQCR13;
		static irqcr_t<0x0008750e> IRQCR14;
		static irqcr_t<0x0008750f> IRQCR15;
	};
	typedef icu_t<void> ICU;

	template <class _> typename icu_t<_>::IR_  icu_t<_>::IR;
	template <class _> typename icu_t<_>::IER_ icu_t<_>::IER;
	template <class _> typename icu_t<_>::IPR_ icu_t<_>::IPR;

	template <class _> typename icu_t<_>::DMRSR0_ icu_t<_>::DMRSR0;
	template <class _> typename icu_t<_>::DMRSR1_ icu_t<_>::DMRSR1;
	template <class _> typename icu_t<_>::DMRSR2_ icu_t<_>::DMRSR2;
	template <class _> typename icu_t<_>::DMRSR3_ icu_t<_>::DMRSR3;
}