//=====================================================//
// RX62Nグループ　ICUユーティリティー
//=====================================================//
#pragma once

#include "../../common/io_utils.hpp"

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 割り込みユーティリティー
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	struct icu_utils {
		//*****************************************************//
		// 選択型割り込み要因選択レジスタテンプレート
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct slixr_t {
			void set(uint16_t idx, uint8_t val) noexcept {
				wr8_(base + idx, val);
			}

			uint8_t get(uint16_t idx) noexcept {
				return rd8_(base + idx);
			}

			volatile uint8_t& operator [] (uint8_t idx) {
				return *reinterpret_cast<volatile uint8_t*>(base + idx);
			}
		};
	};
}