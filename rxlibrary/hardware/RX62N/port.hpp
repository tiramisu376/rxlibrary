//=====================================================//
// RX62Nグループ　ポートレジスター定義
//=====================================================//
#pragma once

#include "../../common/io_utils.hpp"
#include <tuple>

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// ポート定義基底クラス(DDR, DR, PORT, ICR, PCR)
	// param[in]	base   : ベースアドレス
	// param[in]	option : オプション
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <uint32_t base, class option>
	struct port_t : public option {
		static const uint32_t base_adr_ = base;		// ベースアドレス

		//*****************************************************//
		// データディレクションレジスタ(DDR)
		// ポート端子の入出力方向を設定する
		//*****************************************************//
		typedef basic_rw_t<rw8_t<base + 0x00> > DDR_;
		static DDR_ DDR;

		//*****************************************************//
		// データレジスタ(DR)
		// ポート端子への出力データを保存
		//*****************************************************//
		typedef basic_rw_t<rw8_t<base + 0x20> > DR_;
		static DR_ DR;

		//*****************************************************//
		// ポートレジスタ(PORT)
		// ポート端子の状態を保存
		//*****************************************************//
		typedef basic_ro_t<rw8_t<base + 0x40> > PORT_;
		static PORT_ PORT;

		//*****************************************************//
		// 入力バッファコントロールレジスタ(ICR)
		// ポート端子の入力バッファを制御
		//*****************************************************//
		typedef basic_rw_t<rw8_t<base + 0x60> > ICR_;
		static ICR_ ICR;

		//*****************************************************//
		// プルアップ抵抗コントロールレジスタ(PCR)
		// ポート端子のプルアップ抵抗を制御
		//*****************************************************//
		typedef basic_rw_t<rw8_t<base + 0xc0> > PCR_;
		static PCR_ PCR;
	};
	template <uint32_t base, class option> const uint32_t port_t<base, option>::base_adr_;

	template <uint32_t base, class option>
		typename port_t<base, option>::DDR_  port_t<base, option>::DDR;

	template <uint32_t base, class option>
		typename port_t<base, option>::DR_   port_t<base, option>::DR;
	
	template <uint32_t base, class option>
		typename port_t<base, option>::PORT_ port_t<base, option>::PORT;

	template <uint32_t base, class option>
		typename port_t<base, option>::ICR_  port_t<base, option>::ICR;

	template <uint32_t base, class option>
		typename port_t<base, option>::PCR_ port_t<base, option>::PCR;


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// オプション ODR設定非対応ポート用
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <uint32_t base>
	struct odr_x_t {};

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// オプション ODR設定対応ポート用
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <uint32_t base>
	struct odr_o_t {
		//*****************************************************//
		// オープンドレイン制御レジスタ
		// param[in]	offset : レジスタオフセット(将来拡張用)
		//*****************************************************//
		template <uint32_t offset>
		struct odr_t : public rw8_t<offset> {
			typedef rw8_t<offset> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t<io_, bitpos::B0> B0;
			bit_rw_t<io_, bitpos::B1> B1;
			bit_rw_t<io_, bitpos::B2> B2;
			bit_rw_t<io_, bitpos::B3> B3;
			bit_rw_t<io_, bitpos::B4> B4;
			bit_rw_t<io_, bitpos::B5> B5;
			bit_rw_t<io_, bitpos::B6> B6;
			bit_rw_t<io_, bitpos::B7> B7;
		};
		typedef odr_t<base + 0> ODR_;
		static ODR_ ODR;
	};
	template <uint32_t base> typename odr_o_t<base>::ODR_ odr_o_t<base>::ODR;


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// ポート名定義
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	typedef port_t<0x0008c000, odr_o_t<0x0008c080>> PORT0;
	typedef port_t<0x0008c001, odr_o_t<0x0008c081>> PORT1;
	typedef port_t<0x0008c002, odr_o_t<0x0008c082>> PORT2;
	typedef port_t<0x0008c003, odr_o_t<0x0008c083>> PORT3;
	typedef port_t<0x0008c004, odr_x_t<0x0008c084>> PORT4;
	typedef port_t<0x0008c005, odr_x_t<0x0008c085>> PORT5;
	typedef port_t<0x0008c006, odr_x_t<0x0008c086>> PORT6;
	typedef port_t<0x0008c007, odr_x_t<0x0008c087>> PORT7;
	typedef port_t<0x0008c008, odr_x_t<0x0008c088>> PORT8;
	typedef port_t<0x0008c009, odr_x_t<0x0008c089>> PORT9;
	typedef port_t<0x0008c00a, odr_x_t<0x0008c08a>> PORTA;
	typedef port_t<0x0008c00b, odr_x_t<0x0008c08b>> PORTB;
	typedef port_t<0x0008c00c, odr_o_t<0x0008c08c>> PORTC;
	typedef port_t<0x0008c00d, odr_x_t<0x0008c08d>> PORTD;
	typedef port_t<0x0008c00e, odr_x_t<0x0008c08e>> PORTE;
	typedef port_t<0x0008c00f, odr_x_t<0x0008c08d>> PORTF;
	typedef port_t<0x0008c010, odr_x_t<0x0008c08e>> PORTG;


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// ポートの初期設定
	// param[in]	dir : ポート入出力方向
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	inline void init_port(uint8_t dir) {
		PORT0::DDR = dir;		// P00 - P03, P4, P05, P07
		PORT1::DDR = dir;		// P12 - P17
		PORT2::DDR = dir;		// P20 - P27
		PORT3::DDR = dir;		// P30 - P34
		PORT4::DDR = dir;		// P40 - P47
		PORT5::DDR = dir;		// P50 - P57
		PORT6::DDR = dir;		// P60 - P67
		PORT7::DDR = dir;		// P70 - P77
		PORT8::DDR = dir;		// P80 - P85
		PORT9::DDR = dir;		// P90 - P97
		PORTA::DDR = dir;		// PA0 - PA7
		PORTB::DDR = dir;		// PB0 - PB7
		PORTC::DDR = dir;		// PC0 - PC7
		PORTD::DDR = dir;		// PD0 - PD7
		PORTE::DDR = dir;		// PE0 - PE7
		PORTF::DDR = dir;		// PF0 - PF4
		PORTG::DDR = dir;		// PG0 - PG7
	}


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 単ポート定義テンプレート
	// param[in]	PORTX  : ポートクラス
	// param[in]	BPOS   : ビット位置
	// param[in]	ASSERT : アサート論理(通常「1」)
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class PORTX, bitpos BPOS, bool ASSERT = 1>
	struct PORT {
		static const uint8_t PNO	 = static_cast<uint8_t>(PORTX::base_adr_ & 0x1f);
		static const uint8_t BIT_POS = static_cast<uint8_t>(BPOS);

		// オープンドレインタイプ
		enum class OD_TYPE : uint8_t {
			NONE,	// CMOS出力
			N_CH 	// Nチャネル
		};

		// IOマッピングタイプ
		enum class MAP_TYPE : uint8_t {
			IO,			// 汎用入出力端子
			PERIPHERAL	// 周辺機器端子
		};

		//*****************************************************//
		// ポート方向レジスタ
		//*****************************************************//
		typedef bit_rw_t<rw8_t<PORTX::base_adr_ + 0x00>, BPOS> DIR_;
		static DIR_ DIR;

		//*****************************************************//
		// ポートを出力にする
		//*****************************************************//
		static void OUTPUT() {
			DIR = 1;
		}

		//*****************************************************//
		// ポートを入力にする
		//*****************************************************//
		static void INPUT() {
			DIR = 0;
		}

		//*****************************************************//
		// プルアップ制御レジスタ
		//*****************************************************//
		typedef bit_rw_t<rw8_t<PORTX::base_adr_ + 0xc0>, BPOS> PU_;
		static PU_ PU;

		//*****************************************************//
		// オープンドレイン制御レジスタ
		//*****************************************************//
		struct od_t {
			static rw8_t<PORTX::base_adr_ + 0x80> ODR;

			void operator = (OD_TYPE value) {
				uint8_t pos = static_cast<uint8_t>(BPOS);
				if (value) {
					ODR |= 1 << pos;
				} else {
					ODR &= ~(1 << pos);
				}
			}

			OD_TYPE operator () () {
				uint8_t pos = static_cast<uint8_t>(BPOS);
				return static_cast<OD_TYPE>(ODR() & (3 << pos));
			}
		};
		typedef od_t OD_;
		static OD_ OD;

		//*****************************************************//
		// ポートレジスタ
		// ※ポート出力と入力でアドレスが異なる
		//*****************************************************//
		struct port_t {
			typedef bit_rw_t<rw8_t<PORTX::base_adr_ + 0x20>, BPOS> PO_;
			static PO_ PO;	// ポート出力用

			typedef bit_ro_t<ro8_t<PORTX::base_adr_ + 0x40>, BPOS> PI_;
			static PI_ PI;	// ポート入力用

			void operator = (bool value) {
				if (ASSERT) {
					PO = value;
				} else {
					PO = !value;
				}
			}

			bool operator () () {
				if (ASSERT) {
					return PI();
				} else {
					return !PI();
				}
			}
		};
		typedef port_t P_;
		static P_ P;

		//*****************************************************//
		// IOマッピングフラグ
		//*****************************************************//
		struct iomap_t {
			static rw8_t<0x0000f7ec + PNO> MAP;

			void operator = (MAP_TYPE value) {
				uint8_t pos = static_cast<uint8_t>(BPOS);
				if (value) {
					MAP |= 1 << pos;
				} else {
					MAP &= ~(1 << pos);
				}
			}

			MAP_TYPE operator () () {
				uint8_t pos = static_cast<uint8_t>(BPOS);
				return static_cast<MAP_TYPE>(MAP() & (3 << pos));
			}
		};
		typedef iomap_t IOMAP_;
		static IOMAP_ IOMAP;
	};
	template <class PORTX, bitpos BPOS, bool ASSERT>
		typename PORT<PORTX, BPOS, ASSERT>::DIR_ PORT<PORTX, BPOS, ASSERT>::DIR;

	template <class PORTX, bitpos BPOS, bool ASSERT>
		typename PORT<PORTX, BPOS, ASSERT>::OD_ PORT<PORTX, BPOS, ASSERT>::OD;

	template <class PORTX, bitpos BPOS, bool ASSERT>
		typename PORT<PORTX, BPOS, ASSERT>::port_t::PO_ PORT<PORTX, BPOS, ASSERT>::port_t::PO;

	template <class PORTX, bitpos BPOS, bool ASSERT>
		typename PORT<PORTX, BPOS, ASSERT>::port_t::PI_ PORT<PORTX, BPOS, ASSERT>::port_t::PI;

	template <class PORTX, bitpos BPOS, bool ASSERT>
		typename PORT<PORTX, BPOS, ASSERT>::P_ PORT<PORTX, BPOS, ASSERT>::P;

	template <class PORTX, bitpos BPOS, bool ASSERT>
		typename PORT<PORTX, BPOS, ASSERT>::IOMAP_ PORT<PORTX, BPOS, ASSERT>::IOMAP;


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 8ビットポート定義
	// param[in]	PORTx : ポートクラス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class PORTx>
	struct PORT_BYTE {
		//*****************************************************//
		// ポート方向レジスタ
		//*****************************************************//
		typedef rw8_t<PORTx::base_adr_ + 0x00> DIR_;
		static DIR_ DIR;

		//*****************************************************//
		// プルアップ制御レジスタ
		//*****************************************************//
		typedef rw8_t<PORTx::base_adr_ + 0xc0> PU_;
		static PU_ PU;

		//*****************************************************//
		// ポートレジスタ
		// ※ポート出力と入力でアドレスが異なる
		//*****************************************************//
		struct port_t {
			typedef rw8_t<PORTx::base_adr_ + 0x20> PO_;
			static PO_ PO;	// ポート出力用

			typedef ro8_t<PORTx::base_adr_ + 0x40> PI_;
			static PI_ PI;	// ポート入力用

			void operator = (uint8_t value) { PO = value; }
			uint8_t operator () () { return PI(); }
		};
		typedef port_t P_;
		static P_ P;
	};
	template <class PORTx> typename PORT_BYTE<PORTx>::DIR_    PORT_BYTE<PORTx>::DIR;
	template <class PORTx> typename PORT_BYTE<PORTx>::PU_     PORT_BYTE<PORTx>::PU;
	template <class PORTx> typename PORT_BYTE<PORTx>::port_t::PO_ PORT_BYTE<PORTx>::port_t::PO;
	template <class PORTx> typename PORT_BYTE<PORTx>::port_t::PI_ PORT_BYTE<PORTx>::port_t::PI;
	template <class PORTx> typename PORT_BYTE<PORTx>::P_	  PORT_BYTE<PORTx>::P;


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 無効ポート定義
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class _>
	struct NULL_PORT_t {
		static const uint8_t PNO	 = 0xff;
		static const uint8_t BIT_POS = 0xff;

		struct null_t {
			void operator = (bool f) { }
			bool operator () () const { return 0; }
		};

		//*****************************************************//
		// ポート方向レジスタ
		//*****************************************************//
		static null_t DIR;

		//*****************************************************//
		// プルアップレジスタ
		//*****************************************************//
		static null_t PU;

		//*****************************************************//
		// オープンドレイン制御レジスタ
		//*****************************************************//
		static null_t OD;

		//*****************************************************//
		// ポートレジスタ
		//*****************************************************//
		static null_t P;
	};
	typedef NULL_PORT_t<void> NULL_PORT;
	template <class _> typename NULL_PORT_t<_>::null_t NULL_PORT_t<_>::DIR;
	template <class _> typename NULL_PORT_t<_>::null_t NULL_PORT_t<_>::PU;
	template <class _> typename NULL_PORT_t<_>::null_t NULL_PORT_t<_>::OD;
	template <class _> typename NULL_PORT_t<_>::null_t NULL_PORT_t<_>::P;
}