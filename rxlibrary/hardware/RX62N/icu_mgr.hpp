//=====================================================//
// RX62Nグループ　割り込みマネージャー
//=====================================================//
#pragma once

#include "../../common/device.hpp"
#include "../../common/interrupt.h"
#include "../../common/dispatch.hpp"
#include "../RX62N/icu_utils.hpp"

#if !defined(SIG_RX62N)
#error "icu.hpp requires SIG_RX62N to be defined"
#endif

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 割り込みマネージャークラス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class _>
	class icu_mgr_ {
		public:
			//*****************************************************//
			// 割り込みタスクを設定
			// param[in]	vec  : 割り込み要因
			// param[in]	task : 割り込みタスク
			//*****************************************************//
			static void set_task(ICU::VECTOR vec, utils::TASK task) noexcept {
				set_interrupt_task(task, static_cast<uint32_t>(vec));
			}


			//**********************************************************//
			// 割り込みレベルを設定する
			// param[in]	icu	  : 割り込み要因
			// param[in]	level : 割り込みレベル(0の場合、割り込み禁止)
			//**********************************************************//
			static bool set_level(ICU::VECTOR vec, uint8_t level) noexcept {
				bool ena = level != 0 ? true : false;
				switch(vec) {
					case ICU::VECTOR::SWINT:
						ICU::IER.SWINT = 0;
						ICU::IPR.SWINT = level;
						ICU::IER.SWINT = ena;
						break;

					case ICU::VECTOR::CMI0:
						ICU::IER.CMI0 = 0;
						ICU::IPR.CMI0 = level;
						ICU::IER.CMI0 = ena;
						break;

					case ICU::VECTOR::CMI1:
						ICU::IER.CMI1 = 0;
						ICU::IPR.CMI1 = level;
						ICU::IER.CMI1 = ena;
						break;

					case ICU::VECTOR::CMI2:
						ICU::IER.CMI2 = 0;
						ICU::IPR.CMI2 = level;
						ICU::IER.CMI2 = ena;
						break;

					case ICU::VECTOR::CMI3:
						ICU::IER.CMI3 = 0;
						ICU::IPR.CMI3 = level;
						ICU::IER.CMI3 = ena;
						break;

					default:
						return false;
				}
				return true;
			}


			//**********************************************************//
			// 割り込み設定(通常ベクター)
			// param[in]	vec	  : 割り込み要因
			// param[in]	task  : 割り込みタスク
			// param[in]	level : 割り込みレベル(0の場合、割り込み禁止)
			// return		ベクター番号
			//**********************************************************//
			static ICU::VECTOR set_interrupt(ICU::VECTOR vec, utils::TASK task, uint8_t level) noexcept {
				set_task(vec, task);
				set_level(vec, level);
				return vec;
			}


			//**********************************************************//
			// 割り込みレベルを設定する
			// param[in]	per	  : 周辺機器タイプ
			// param[in]	level : 割り込みレベル(0の場合、割り込み禁止)
			// return		成功なら「true」
			//**********************************************************//
			static bool set_level(peripheral per, uint8_t level) noexcept {
				bool ena = level != 0 ? true : false;
				switch(per) {
					case peripheral::RIIC0:
						ICU::IPR.ICRXI0 = level;
						ICU::IER.ICRXI0 = ena;
						ICU::IPR.ICTXI0 = level;
						ICU::IER.ICTXI0 = ena;
						break;

					case peripheral::RIIC1:
						ICU::IPR.ICRXI1 = level;
						ICU::IER.ICRXI1 = ena;
						ICU::IPR.ICTXI1 = level;
						ICU::IER.ICTXI1 = ena;
						break;

					case peripheral::SCI0:
						ICU::IPR.RXI0 = level;
						ICU::IER.RXI0 = ena;
						ICU::IPR.TXI0 = level;
						ICU::IER.TXI0 = ena;
						break;

					case peripheral::SCI1:
						ICU::IPR.RXI1 = level;
						ICU::IER.RXI1 = ena;
						ICU::IPR.TXI1 = level;
						ICU::IER.TXI1 = ena;
						break;

					case peripheral::SCI2:
						ICU::IPR.RXI2 = level;
						ICU::IER.RXI2 = ena;
						ICU::IPR.TXI2 = level;
						ICU::IER.TXI2 = ena;
						break;

					case peripheral::SCI3:
						ICU::IPR.RXI3 = level;
						ICU::IER.RXI3 = ena;
						ICU::IPR.TXI3 = level;
						ICU::IER.TXI3 = ena;
						break;

					case peripheral::SCI5:
						ICU::IPR.RXI5 = level;
						ICU::IER.RXI5 = ena;
						ICU::IPR.TXI5 = level;
						ICU::IER.TXI5 = ena;
						break;

					case peripheral::SCI6:
						ICU::IPR.RXI6 = level;
						ICU::IER.RXI6 = ena;
						ICU::IPR.TXI6 = level;
						ICU::IER.TXI6 = ena;
						break;

					default:
						return false;
				}
				return true;
			}
	};
	typedef icu_mgr_<void> icu_mgr;
}