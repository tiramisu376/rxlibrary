//=====================================================//
// RX62Nグループ　システム制御
//=====================================================//
#pragma once

#include "system.hpp"

#if !defined(F_ICLK) || !defined(F_PCLK) || !defined(F_BCLK)
#error "system_clk.hpp requires F_ICLK, F_PCLK, F_BCLK to be defined"
#endif

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// system_ioクラス
	// param[in]	BASE_CLOCK : ベースクロック(デフォルト 12MHz)
	// param[in]	EXT_CLOCK  : 外部クロック出力を行うか(デフォルト false)
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <uint32_t BASE_CLOCK = 12000000, bool EXT_CLOCK = false>
	struct system_io {
		static uint8_t clock_multiply(uint32_t clk) noexcept {
			uint8_t multiply = 3;
			while(clk > BASE_CLOCK) {
				multiply--;
				clk >>= 1;
			}
			return multiply;
		}

		//*****************************************************//
		// システムクロックの設定
		//*****************************************************//
		static void setup_system_clock() noexcept {
			device::SYSTEM::SCKCR = device::SYSTEM::SCKCR.ICK.bit(clock_multiply(F_ICLK)) |
									device::SYSTEM::SCKCR.BCK.bit(clock_multiply(F_BCLK)) |
									device::SYSTEM::SCKCR.PCK.bit(clock_multiply(F_PCLK));

		}
	};
}