//=====================================================//
// RX62Nグループ　SCI定義
//=====================================================//
#pragma once

#include "../../common/device.hpp"

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// SCI定義基底クラス
	// param[in]	base : ベースアドレス
	// param[in]	per  : ペリフェラル型
	// param[in]	txv	 : 送信割り込みベクター
	// param[in]	rxv	 : 受信割り込みベクター
	// param[in]	INT  : 送信割り込みベクター型
	// param[in]	tev	 : 送信終了割り込みベクター
	// param[in]	pclk : PCLK周波数
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
	struct sci_t {
		static const auto PERIPHERAL = per;		// ペリフェラル型
		static const auto TX_VEC = txv;			// 送信割り込みベクター
		static const auto RX_VEC = rxv;			// 受信割り込みベクター
		static const auto TE_VEC = tev;			// 送信終了割り込みベクター
		static const uint32_t PCLK = pclk;		// PCLK周波数

		//*****************************************************//
		// レシーブデータレジスタ(RDR)
		//*****************************************************//
		typedef ro8_t<base + 0x05> RDR_;
		static RDR_ RDR;


		//*****************************************************//
		// トランスミットデータレジスタ(TDR)
		//*****************************************************//
		typedef rw8_t<base + 0x03> TDR_;
		static TDR_ TDR;


		//*****************************************************//
		// シリアルモードレジスタ(SMR)
		// param[in]	offset : レジスタオフセット
		//*****************************************************//
		template <uint32_t offset>
		struct smr_t : rw8_t<offset> {
			typdef rw8_t<offset> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			// 共通
			bits_rw_t<io_, bitpos::B0, 2> CKS;
			bit_rw_t <io_, bitpos::B4>	  PM;
			bit_rw_t <io_, bitpos::B5>	  PE;

			// シリアルコミュニケーションインタフェースモード時
			bit_rw_t <io_, bitpos::B2>	  MP;
			bit_rw_t <io_, bitpos::B3>	  STOP;
			bit_rw_t <io_, bitpos::B6>	  CHR;
			bit_rw_t <io_, bitpos::B7>	  CM;

			// スマートカードインタフェースモード時
			bits_rw_t<io_, bitpos::B2, 2> BCP;
			bit_rw_t <io_, bitpos::B6>	  BLK;
			bit_rw_t <io_, bitpos::B7>	  GM;
		};
		typedef smr_t<base + 0x00> SMR_;
		static SMR_ SMR;


		//*****************************************************//
		// シリアルコントロールレジスタ(SCR)
		// param[in]	offset : レジスタオフセット
		//*****************************************************//
		template <uint32_t offset>
		struct scr_t : public rw8_t<offset> {
			typedef rw8_t<offset> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bits_rw_t<io_, bitpos::B0, 2> CKE;
			bit_rw_t <io_, bitpos::B2>	  TEIE;
			bit_rw_t <io_, bitpos::B3>	  MPIE;
			bit_rw_t <io_, bitpos::B4>	  RE;
			bit_rw_t <io_, bitpos::B5>	  TE;
			bit_rw_t <io_, bitpos::B6>	  RIE;
			bit_rw_t <io_, bitpos::B7>	  TIE;
		};
		typedef scr_t<base + 0x02> SCR_;
		static SCR_ SCR;

		//*****************************************************//
		// シリアルステータスレジスタ(SSR)
		// param[in]	offset : レジスタオフセット
		//*****************************************************//
		template <uint32_t offset>
		struct ssr_t : public rw8_t<offset> {
			typedef rw8_t<offset> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			// 共通
			bit_rw_t<io_, bitpos::B0> MPBT;
			bit_rw_t<io_, bitpos::B1> MPB;
			bit_rw_t<io_, bitpos::B2> TEND;
			bit_rw_t<io_, bitpos::B3> PER;
			bit_rw_t<io_, bitpos::B5> ORER;
			bit_rw_t<io_, bitpos::B6> RDRF;
			bit_rw_t<io_, bitpos::B7> TDRE;

			// シリアルコミュニケーションインタフェースモード時
			bit_rw_t<io_, bitpos::B4> FER;

			// スマートカードインタフェースモード時
			bit_rw_t<io_, bitpos::B4> ERS;
		};
		typedef ssr_t<base + 0x04> SSR_;
		static SSR_ SSR;


		//*****************************************************//
		// スマートカードモードレジスタ(SCMR)
		// param[in]	offset : レジスタオフセット
		//*****************************************************//
		template <uint32_t offset>
		struct scmr_t : public rw8_t<offset> {
			typedef rw8_t<offset> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t<io_, bitpos::B0> SMIF;
			bit_rw_t<io_, bitpos::B2> SINV;
			bit_rw_t<io_, bitpos::B3> SDIR;
			bit_rw_t<io_, bitpos::B7> BCP2;
		};
		typedef scmr_t<base + 0x06> SCMR_;
		static SCMR_ SCMR;


		//*****************************************************//
		// ビットレートレジスタ(BRR)
		//*****************************************************//
		typedef rw8_t<base + 0x01> BRR_;
		static BRR_ BRR;


		//*****************************************************//
		// シリアル拡張レジスタ(SEMR)
		// param[in]	offset : レジスタオフセット
		//*****************************************************//
		template <uint32_t offset>
		struct semr_t : public rw8_t<offset> {
			typedef rw8_t<offset> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t<io_, bitpos::B0> ACS0;
			bit_rw_t<io_, bitpos::B4> ABCS;
		};
		typedef semr_t<base + 0x06> SEMR_;
		static SEMR_ SEMR;
	};
	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
		typename sci_t<base, per, txv, rxv, INT, tev, pclk>::RDR_ sci_t<base, per, txv, rxv, INT, tev, pclk>::RDR;

	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
		typename sci_t<base, per, txv, rxv, INT, tev, pclk>::TDR_ sci_t<base, per, txv, rxv, INT, tev, pclk>::TDR;

	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
		typename sci_t<base, per, txv, rxv, INT, tev, pclk>::SMR_ sci_t<base, per, txv, rxv, INT, tev, pclk>::SMR;

	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
		typename sci_t<base, per, txv, rxv, INT, tev, pclk>::SCR_ sci_t<base, per, txv, rxv, INT, tev, pclk>::SCR;

	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
		typename sci_t<base, per, txv, rxv, INT, tev, pclk>::SSR_ sci_t<base, per, txv, rxv, INT, tev, pclk>::SSR;

	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
		typename sci_t<base, per, txv, rxv, INT, tev, pclk>::SCMR_ sci_t<base, per, txv, rxv, INT, tev, pclk>::SCMR;

	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
		typename sci_t<base, per, txv, rxv, INT, tev, pclk>::BRR_ sci_t<base, per, txv, rxv, INT, tev, pclk>::BRR;

	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
		typename sci_t<base, per, txv, rxv, INT, tev, pclk>::SEMR_ sci_t<base, per, txv, rxv, INT, tev, pclk>::SEMR;


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// SCIaクラス
	// param[in]	base : ベースアドレス
	// param[in]	per  : ペリフェラル型
	// param[in]	txv	 : 送信割り込みベクター
	// param[in]	rxv	 : 受信割り込みベクター
	// param[in]	tev	 : 送信終了割り込みベクター
	// param[in]	pclk : PCLK周波数
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <uint32_t base, peripheral per, ICU::VECTOR txv, ICU::VECTOR rxv, typename INT, INT tev, uint32_t pclk>
	struct scia_t : public sci_t<base, per, txv, rxv, INT, tev, pclk> {
	};

	typedef scia_t<0x0008240, peripheral::SCI0, ICU::VECTOR::TXI0, ICU::VECTOR::RXI0, ICU::VECTOR, ICU::VECTOR::TEI0, F_PCLK> SCI0;
	typedef scia_t<0x0008248, peripheral::SCI1, ICU::VECTOR::TXI1, ICU::VECTOR::RXI1, ICU::VECTOR, ICU::VECTOR::TEI1, F_PCLK> SCI1;
	typedef scia_t<0x000824f, peripheral::SCI2, ICU::VECTOR::TXI2, ICU::VECTOR::RXI2, ICU::VECTOR, ICU::VECTOR::TEI2, F_PCLK> SCI2;
	typedef scia_t<0x0008258, peripheral::SCI3, ICU::VECTOR::TXI3, ICU::VECTOR::RXI3, ICU::VECTOR, ICU::VECTOR::TEI3, F_PCLK> SCI3;
	typedef scia_t<0x0008268, peripheral::SCI5, ICU::VECTOR::TXI5, ICU::VECTOR::RXI5, ICU::VECTOR, ICU::VECTOR::TEI5, F_PCLK> SCI5;
	typedef scia_t<0x0008270, peripheral::SCI6, ICU::VECTOR::TXI6, ICU::VECTOR::RXI6, ICU::VECTOR, ICU::VECTOR::TEI6, F_PCLK> SCI6;
}
