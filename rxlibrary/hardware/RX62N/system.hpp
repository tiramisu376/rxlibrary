//=====================================================//
// RX62Nグループ　システム定義
//=====================================================//
#pragma once

#include "../../common/io_utils.hpp"

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// システム定義ベースクラス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class _>
	struct system_t {
		//*****************************************************//
		// モードモニタレジスタ(MDMONR)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct mdmonr_t : public rw16_t<base> {
			typedef rw16_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t<io_, bitpos::B0> MD0;
			bit_rw_t<io_, bitpos::B1> MD1;
			bit_rw_t<io_, bitpos::B7> MDE;
		};
		typedef mdmonr_t<0x0080000> MDMONR_;
		static MDMONR_ MDMONR;


		//*****************************************************//
		// モードステータスレジスタ(MDSR)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct mdsr_t : public rw16_t<base> {
			typedef rw16_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t <io_, bitpos::B0>	  IROM;
			bit_rw_t <io_, bitpos::B1>	  EXB;
			bits_rw_t<io_, bitpos::B2, 2> BSW;
			bit_rw_t <io_, bitpos::B4>	  BOTS;
			bit_rw_t <io_, bitpos::B6>	  UBTS;
		};
		typedef mdsr_t<0x0080002> MDSR_;
		static MDSR_ MDSR;


		//*****************************************************//
		// システムコントロールレジスタ0(SYSCR0)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct syscr0_t : public rw16_t<base> {
			typedef rw16_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t <io_, bitpos::B0>	  ROME;
			bit_rw_t <io_, bitpos::B1>	  EXBE;
			bits_rw_t<io_, bitpos::B8, 8> KEY;
		};
		typedef syscr0_t<0x0080006> SYSCR0_;
		static SYSCR0_ SYSCR0;


		//*****************************************************//
		// システムコントロールレジスタ1(SYSCR1)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct syscr1_t : public rw16_t<base> {
			typedef rw16_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t <io_, bitpos::B0> RAME;
		};
		typedef syscr1_t<0x0080008> SYSCR1_;
		static SYSCR1_ SYSCR1;


		//*****************************************************//
		// モジュールストップコントロールレジスタA(MSTPCRA)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct mstpcra_t : public rw32_t<base> {
			typedef rw32_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t<io_, bitpos::B4>  MSTPA4;
			bit_rw_t<io_, bitpos::B5>  MSTPA5;
			bit_rw_t<io_, bitpos::B8>  MSTPA8;
			bit_rw_t<io_, bitpos::B9>  MSTPA9;
			bit_rw_t<io_, bitpos::B10> MSTPA10;
			bit_rw_t<io_, bitpos::B11> MSTPA11;
			bit_rw_t<io_, bitpos::B14> MSTPA14;
			bit_rw_t<io_, bitpos::B15> MSTPA15;
			bit_rw_t<io_, bitpos::B17> MSTPA17;
			bit_rw_t<io_, bitpos::B19> MSTPA19;
			bit_rw_t<io_, bitpos::B22> MSTPA22;
			bit_rw_t<io_, bitpos::B23> MSTPA23;
			bit_rw_t<io_, bitpos::B28> MSTPA28;
			bit_rw_t<io_, bitpos::B29> MSTPA29;
			bit_rw_t<io_, bitpos::B31> ACSE;
		};
		typedef mstpcra_t<0x0080010> MSTPCRA_;
		static MSTPCRA_ MSTPCRA;


		//*****************************************************//
		// モジュールストップコントロールレジスタB(MSTPCRB)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct mstpcrb_t : public rw32_t<base> {
			typedef rw32_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t<io_, bitpos::B0>  MSTPB0;
			bit_rw_t<io_, bitpos::B15> MSTPB15;
			bit_rw_t<io_, bitpos::B16> MSTPB16;
			bit_rw_t<io_, bitpos::B17> MSTPB17;
			bit_rw_t<io_, bitpos::B18> MSTPB18;
			bit_rw_t<io_, bitpos::B19> MSTPB19;
			bit_rw_t<io_, bitpos::B20> MSTPB20;
			bit_rw_t<io_, bitpos::B21> MSTPB21;
			bit_rw_t<io_, bitpos::B23> MSTPB23;
			bit_rw_t<io_, bitpos::B25> MSTPB25;
			bit_rw_t<io_, bitpos::B26> MSTPB26;
			bit_rw_t<io_, bitpos::B28> MSTPB28;
			bit_rw_t<io_, bitpos::B29> MSTPB29;
			bit_rw_t<io_, bitpos::B30> MSTPB30;
			bit_rw_t<io_, bitpos::B31> MSTPB31;
		};
		typedef mstpcrb_t<0x0080010> MSTPCRB_;
		static MSTPCRB_ MSTPCRB;
		

		//*****************************************************//
		// モジュールストップコントロールレジスタC(MSTPCRC)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct mstpcrc_t : public rw32_t<base> {
			typedef rw32_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t<io_, bitpos::B0> MSTPC0;
			bit_rw_t<io_, bitpos::B1> MSTPC1;
		};
		typedef mstpcrc_t<0x0080010> MSTPCRC_;
		static MSTPCRC_ MSTPCRC;


		//*****************************************************//
		// システムクロックコントロールレジスタ(SCKCR)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct sckcr_t : public rw32_t<base> {
			typedef rw32_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bits_rw_t<io_, bitpos::B8,  4> PCK;
			bits_rw_t<io_, bitpos::B16, 4> BCK;
			bit_rw_t <io_, bitpos::B22>	   PSTOP0;
			bit_rw_t <io_, bitpos::B23>	   PSTOP1;
			bits_rw_t<io_, bitpos::B24, 4> ICK;
		};
		typedef sckcr_t<0x0080020> SCKCR_;
		static SCKCR_ SCKCR;


		//*****************************************************//
		// 外部クロックコントロールレジスタ(BCKCR)
		// param[in]	base : ベースアドレス
		//*****************************************************//
		template <uint32_t base>
		struct bckcr_t : public rw8_t<base> {
			typedef rw8_t<base> io_;
			using io_::operator =;
			using io_::operator ();
			using io_::operator |=;
			using io_::operator &=;

			bit_rw_t<io_, bitpos::B0> BCLKDIV;
		};
		typedef bckcr_t<0x0080030> BCKCR_;
		static BCKCR_ BCKCR;
	};
	typedef system_t<void> SYSTEM;

	template <class _> typename system_t<_>::MDMONR_  system_t<_>::MDMONR;
	template <class _> typename system_t<_>::MDSR_	  system_t<_>::MDSR;
	template <class _> typename system_t<_>::SYSCR0_  system_t<_>::SYSCR0;
	template <class _> typename system_t<_>::SYSCR1_  system_t<_>::SYSCR1;
	template <class _> typename system_t<_>::MSTPCRA_ system_t<_>::MSTPCRA;
	template <class _> typename system_t<_>::MSTPCRB_ system_t<_>::MSTPCRB;
	template <class _> typename system_t<_>::MSTPCRC_ system_t<_>::MSTPCRC;
	template <class _> typename system_t<_>::SCKCR_	  system_t<_>::SCKCR;
	template <class _> typename system_t<_>::BCKCR_	  system_t<_>::BCKCR;
}