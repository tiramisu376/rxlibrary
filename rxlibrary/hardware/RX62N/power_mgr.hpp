//=====================================================//
// RX62Nグループ　省電力制御
//=====================================================//
#pragma once

#include "peripheral.hpp"
#include "system.hpp"
//#include "bus.hpp"
#include "../../common/static_holder.hpp"

#if !defined(SIG_RX62N)
#error "power_mgr.hpp requires SIG_RX62N to be defined"
#endif

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 電力制御クラス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	class power_mgr {
		private:
			struct pad_t {
				uint8_t cmt_;

				pad_t() : cmt_(0) {}
			};

			typedef utils::static_holder<pad_t> STH;

			static void set_(bool f, uint8_t& pad, peripheral org, peripheral tgt) {
				if (f) {
					pad |= 1 << (static_cast<uint16_t>(tgt) - static_cast<uint16_t>(org));
				} else {
					pad &= ~(1 << (static_cast<uint16_t>(tgt) - static_cast<uint16_t>(org)));
				}
			}

		public:
			//*****************************************************//
			// 周辺機器に切り替える
			// param[in]	t   : 周辺機器タイプ
			// param[in]	ena : オフにする場合「false」
			//*****************************************************//
			static void turn(peripheral t, bool ena = true) {
				bool f = !ena;

				switch(t) {
					case peripheral::CMT0:
					case peripheral::CMT1:
						// CMT0, CMT1のストップ状態設定
						set_(ena, STH::st.cmt_, peripheral::CMT0, t);
						SYSTEM::MSTPCRA.MSTPA15 = ((STH::st.cmt_ & 0b0011) == 0);
						break;

					case peripheral::CMT2:
					case peripheral::CMT3:
						// CMT2, CMT3のストップ状態設定
						set_(ena, STH::st.cmt_, peripheral::CMT0, t);
						SYSTEM::MSTPCRA.MSTPA14 = ((STH::st.cmt_ & 0b1100) == 0);
						break;

					case peripheral::SCI0:
						SYSTEM::MSTPCRB.MSTPB31 = f;	// SCI0のストップ状態解除
						break;

					case peripheral::SCI1:
						SYSTEM::MSTPCRB.MSTPB30 = f;	// SCI1のストップ状態解除
						break;

					case peripheral::SCI2:
						SYSTEM::MSTPCRB.MSTPB29 = f;	// SCI2のストップ状態解除
						break;

					case peripheral::SCI3:
						SYSTEM::MSTPCRB.MSTPB28 = f;	// SCI3のストップ状態解除
						break;

					case peripheral::SCI5:
						SYSTEM::MSTPCRB.MSTPB26 = f;	// SCI5のストップ状態解除
						break;

					case peripheral::SCI6:
						SYSTEM::MSTPCRB.MSTPB25 = f;	// SCI6のストップ状態解除
						break;
				}
			}
	};
}