//=====================================================//
// RX62Nグループ　ポートマッピング
//=====================================================//
#pragma once

#include "peripheral.hpp"
#include "port.hpp"

#if !defined(SIG_RX62N)
#error "port_map.hpp requires SIG_RX62N to be defined"
#endif

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// ポートマッピングユーティリティ
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	class port_map {
		public:
			//*****************************************************//
			// ポートマッピングオプション型
			//*****************************************************//
			enum class option : uint8_t {
				FIRST,		// 第1候補
				SECOND,		// 第2候補
				THIRD,		// 第3候補
				FORCE,		// 第4候補
				LOCAL0,		// 独自設定0
				LOCAL1		// 独自設定1
			};


			//*****************************************************//
			// MTUx(マルチファンクションタイマ) チャネル型
			//*****************************************************//
			enum class channel : uint8_t {
				A,		// MTUx A (MTIOCxA)
				B,		// MTUx B (MTIOCxB)
				C,		// MTUx C (MTIOCxC)
				D,		// MTUx D (MTIOCxD)

				U,		// MTU5 U (MTIC5U)
				V,		// MTU5 V (MTIC5V)
				W,		// MTU5 W (MTIC5W)

				CLK_AB,	// MTCLKA, MTCLKB
				CLK_CD	// MTCLKC, MTCLKD
			};

		private:
			static bool sub_1st(peripheral t, bool enable, option opt) noexcept {
				bool ret = true;

				switch(t) {
					case peripheral::SCI0:

					default:
						ret = false;
				}
				return ret;
			}
	};
}