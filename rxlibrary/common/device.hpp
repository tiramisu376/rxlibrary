//=====================================================//
// RXマイコン デバイス固有ヘッダー
//=====================================================//
#pragma once

#include "io_utils.hpp"

#include "../hardware/RX62N/peripheral.hpp"
#include "../hardware/RX62N/system.hpp"
#include "../hardware/RX62N/power_mgr.hpp"
#include "../hardware/RX62N/icu.hpp"
#include "../hardware/RX62N/icu_mgr.hpp"
#include "../hardware/RX62N/port_map.hpp"