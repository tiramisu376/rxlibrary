//=====================================================//
// 割り込み関係処理
//=====================================================//
#include <stdlib.h>
#include "interrupt.h"

void (*interrupt_vectors[256])(void);

// 割り込み関数の初期値
INTERRUPT_FUNC void null_func(void) {
}

// 特権命令例外処理
INTERRUPT_FUNC void _rx_priviledged_exception_handler(void) {
}

// アクセス例外処理
INTERRUPT_FUNC void _rx_access_exception_handler(void) {
}

// 未定義命令例外処理
INTERRUPT_FUNC void _rx_undefined_exception_handler(void) {
}

// 浮動小数点例外処理
INTERRUPT_FUNC void _rx_floating_exception_handler(void) {
}

// ノンマスカブル割り込み処理
INTERRUPT_FUNC void _rx_nonmaskable_exception_handler(void) {
}


//*****************************************************//
// 割り込みベクタテーブルの初期化
//*****************************************************//
void init_interrupt(void) {
	for(int i = 0; i < 256; i++) {
		interrupt_vectors[i] = null_func;
	}
}


//*****************************************************//
// 割り込み関数の設定
// param[in]	interrupt_func : 割り込み関数
// param[in]	vector		   : 割り込みベクタ番号
// return		なし
//*****************************************************//
void set_interrupt_task(void (*interrupt_task)(void), uint32_t vector) {
	if (vector < 256) {
		if (interrupt_task == NULL) {
			interrupt_task = null_func;
		}
		interrupt_vectors[vector] = interrupt_task;
	}
}