//===========================================
// Renesas RXデバイス用ライブラリー
//===========================================
#pragma once

#include "interrupt.h"
#include "device.hpp"

// RX62N用
#include "../hardware/RX62N/system.hpp"
#include "../hardware/RX62N/system_io.hpp"

// 共通化する予定のもの
#include "../hardware/RX62N/port.hpp"

// 共通
#include "../hardware/RX62N/cmt.hpp"
#include "../hardware/RX62N/sci.hpp"