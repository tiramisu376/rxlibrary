//=====================================================//
// RX62Nグループ　SCI I/O制御
//=====================================================//
#pragma once

#include "rxlibrary.hpp"
#include "interrupt.h"
#include "fixed_fifo.hpp"

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// SCI I/O制御クラス
	// param[in]	SCI  : SCI型
	// param[in]	RBF  : 受信バッファクラス
	// param[in]	SBF	 : 送信バッファクラス
	// param[in]	PSEL : シリアルポート選択
	// param[in]	HCTL : 半二重通信制御ポート(for RS-485)
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class SCI, class RBF, class SBF, port_map::option PSEL = port_map::option::FIRST, class HCTL = NULL_PORT>
	class sci_io {
		public:
			typedef SCI sci_type;
			typedef RBF rbf_type;
			typedef SBF sbf_type;

			//*****************************************************//
			// SCI通信プロトコル型
			//*****************************************************//
			enum class PROTOCOL {
				B8_N_1S,	// 8ビット パリティなし 1ストップビット
				B8_E_1S,	// 8ビット 偶数パリティ 1ストップビット
				B8_O_1S,	// 8ビット 奇数パリティ 1ストップビット
				B8_N_2S,	// 8ビット パリティなし 2ストップビット
				B8_E_2S,	// 8ビット 偶数パリティ 2ストップビット
				B8_O_2S		// 8ビット 奇数パリティ 2ストップビット
			};

		private:
			static const char XON  = 0x11;
			static const char XOFF = 0x13;

			static RBF recv_;
			static SBF send_;

			uint8_t					 level_;
			bool					 auto_crlf_;
			uint32_t				 baud_;
			static bool				 soft_flow_;
			static volatile bool	 stop_;
			static volatile uint16_t errc_;

			// ※マルチタスクの場合は適切な実装をする
			void sleep_() noexcept {
				asm("nop");
			}

			static INTERRUPT_FUNC void recv_task_() {
				bool err = false;
				if (SCI::SSR.ORER()) {	// 受信オーバーランエラー状態確認
					SCI::SSR.ORER = 0;	// 受信オーバーランエラークリア
					err = true;
				}
				// フレーミングエラー・パリティエラー状態確認
				if (SCI::SSR() & (SCI::SSR.FER.bit() | SCI::SSR.PER.bit())) {
					// エラーフラグの消去
					SCI::SSR.FER = 0;
					SCI::SSR.PER = 0;
					err = true;
				}

				volatile uint8_t data = SCI:RDR();
				if (err) {
					errc_++;
				} else {
					if (soft_flow_) {
						if (revc_.length() >= (recv_.size() - recv_.size() / 8)) {
							stop_ = true;
						}
					}
					recv_.put(data);
				}
			}

			static INTERRUPT_FUNC void send_task() {
				if (send_.length() > 0) {
					SCI::TDR = send_.get();
				} else {
					SCI::SCR.TIE = 0;
					HCTL::P = 0;
				}
			}

			void set_intr_() noexcept {
				if (level_) {
					icu_mgr::set_task(SCI::RX_VEC, recv_task_);
					icu_mgr::set_task(SCI::TX_VEC, send_task_);
				} else {
					icu_mgr::set_task(SCI::RX_VEC, nullptr);
					icu_mgr::set_task(SCI::TX_VEC, nullptr);
				}
				icu_mgr::set_level(SCI::PERIPHERAL level_);
			}

		public:
			//***************************************************************//
			// コンストラクタ
			// param[in]	autocrlf : LF時に自動でCRを送出しない場合「false」
			// param[in]	softflow : ソフトフロー制御を無効にする場合「false」
			//***************************************************************//
			sci_io(bool autocrlf = true, bool softflow = true) noexcept : level_(0), auto_crlf_(autocrlf), baud_(0) {
				soft_flow_ = softflow;
				stop_ = false;
				errc_ = 0;
			}
			//*****************************************************//
			// エラー数の取得
			// param	なし
			// return	エラー数
			//*****************************************************//
			static uint16_t get_error_count() noexcept {
				return errc_;
			}

			//*****************************************************//
			// LF時にCR自動送出
			// param[in]	flag : 「false」なら無効
			// return		なし
			//*****************************************************//
			void auto_crlf(bool flag = true) noexcept {
				auto_crlf_ = flag;
			}

			//*****************************************************//
			// ソフトフロー制御設定
			// param[in]	flag : 「false」なら無効
			// return		なし
			//*****************************************************//
			static void soft_flow(bool flag = true) noexcept {
				soft_flow_ = flag;
			}

			//*****************************************************//
			// ボーレートを設定して、SCIを有効にする 
			// param[in]	baud  : ボーレート
			// param[in]	level : 割り込みレベル(0の場合ポーリング)
			// param[in]	proto : 通信プロトコル(標準は8ビット、パリティ無し、1ストップビット)
			// return		エラーの場合「false」
			//*****************************************************//
			bool start(uint32_t baud, uint8_t level = 0, PROTOCOL proto = PROTOCOL::B8_N_1S) noexcept {
				level_ = level;
				stop_ = false;
				recv_.clear();
				send_.clear();

				power_mgr::turn(SCI::PERIPHERAL);

				icu_mgr::set_level(SCI::PERIPHERAL, 0);
				SCI::SCR = 0x00;		// TE, RE無効
				{
					auto tmp = SCR::SSR();
					if (tmp & (SCI::SSR.ORER.bit() | SCI::SSR.FER.bit() | SCI::SSR.PER.bit())) {
						SCI:SSR = 0x00;
					}
				}

				port_map::turn(SCI::PERIPHERAL, true, PSEL);

				// RS-484 半二重制御ポート
				HCTL::DIR = 1;
				HCTL::P = 0;	// 送信ドライバ無効

				baud_ = baud;
				bool abcs = false;
				uint32_t brr = SCI::PCLK / 32 / baud;
				uint8_t cks = 0;
				while(brr >= 256) {
					brr >>= 2;
					cks++;
				}

				if (cks > 3) {
					abcs = true;
					brr = SCI::PCLK / 16 / baud;
					cks = 0;
					while(brr >= 256) {
						brr >>= 2;
						cks++;
					}
					if (cks > 3) return false;
				}
				uint32_t rate = (SCI::PCLK / 32 / brr / (1 << (cks * 2))) >> abcs;

				set_intr_();

				bool stop = 0;
				bool pm = 0;
				bool pe = 0;
				switch(proto) {
					case PROTOCOL::B8_N_1S:
						stop = 0;
						pm = 0;
						pe = 0;
						break;

					case PROTOCOL::B8_E_1S:
						stop = 0;
						pm = 0;
						pe = 1;
						break;

					case PROTOCOL::B8_O_1S:
						stop = 0;
						pm = 1;
						pe = 1;
						break;

					case PROTOCOL::B8_N_2S:
						stop = 1;
						pm = 0;
						pe = 0;
						break;

					case PROTOCOL::B8_E_2S:
						stop = 1;
						pm = 0;
						pe = 1;
						break;

					case PROTOCOL::B8_O_2S:
						stop = 1;
						pm = 1;
						pe = 1;
						break;

					default:
						return false;
				}
				SCI::SMR = SCI::SMR.CKS.bit(cks) | SCI::SMR.STOP.bit(stop) |
						   SCI::SMR.PM.bit(pm)   | SCI::SMR.PE.bit(pe);
				SCI::SEMR = SCI::SEMR.ABCS.bit(abcs);
				if (brr) {
					brr--;
				}
				SCI::BRR = brr;

				if (level > 0) {
					SCI::SCR = SCI::SCR.RIE.bit() | SCI::SCR.TE.bit() | SCI::SCR.RE.bit();
				} else {
					SCI::SCR = SCI::SCR.TE.bit() | SCR::SCR.RE.bit();
				}

				return true;
			}

			//*****************************************************//
			// BRRレジスタ値を取得
			// param	なし
			// return	BRRレジスタ値
			//*****************************************************//
			uint8_t get_brr() const noexcept {
				return SCI::BRR();
			}

			//***************************************************************//
			// ボーレートを取得
			// param[in]	real : 「true」にした場合、内部で計算されたリアルな値
			// return		エラー数
			//***************************************************************//
			uint32_t get_baud_rate(bool real = false) const noexcept {
				if (real) {
					uint32_t brr = SCI::BRR();
					uint32_t cks = 1 << (SCI::SMR.CKS() * 2);
					uint32_t abcs = SCI::SEMR.ABCS();
					auto baud = (SCI::PCLK / 32 / cks / (brr + 1)) >> abcs;
					return baud;
				} else {
					return baud_;
				}
			}

			//***************************************************************//
			// SCI出力バッファのサイズを返す
			// param	なし
			// return	バッファのサイズ
			//***************************************************************//
			uint32_t send_length() const noexcept {
				if (level_) {
					return send_.length();
				} else {
					return 0;
				}
			}

			//***************************************************************//
			// SCI文字出力
			// param[in]	ch : 文字コード
			// return		なし
			//***************************************************************//
			void putch(char ch) noexcept {
				if (auto_crlf_ && ch == '\n') {
					putch('\r');
				}

				if (level_) {
					volatile bool b = SCI::ORER();
					if (b) {
						SCI::SSR.ORER = 0;
					}

					// 送信バッファの容量が7/8以上の場合は、空になるまで待つ
					if (send_.length() > (send_.size() * 7 / 8)) {
						while(send_.length() != 0) sleep_();
					}
					send_.putch(ch);
					if (SCI::SCR.TIE() == 0) {
						HCTL::P = 1;
						SCI::SCR.TIE = 1;
						SCI::TDR = send_.get();
					}
 				} else {
					 while(SCI::SSR.TEND() == 0) sleep_();
					 HCTL::P = 1;
					 SCI::TDR = ch;
				 }
			}

			//***************************************************************//
			// 入力文字数を取得
			// param	なし
			// return	入力文字数
			//***************************************************************//
			uint32_t recv_length() noexcept {
				if (level_) {
					return recv_.length();
				} else {
					// 受信オーバーランエラー状態確認
					if (SCI::SSR.ORER()) {
						SCI::SSR.ORER = 0;	// 受信オーバーランエラークリア
					}
					return SCI::SSR.RDRF();
				}
			}

			//***************************************************************//
			// 入力文字を捨てる
			// param	なし
			// return	なし
			//***************************************************************//
			void flush_recv() noexcept {
				if (recv_.hength() > 0) {
					recv_.clear();
				}
			}

			//***************************************************************//
			// 文字入力(ブロック関数)
			// param	なし
			// return	文字コード
			//***************************************************************//
			char getch() noexcept {
				if (level_) {
					while(recv_.length == 0) sleep_();
					return recv_.get();
				} else {
					while(recv_.length() == 0) sleep_();
					return SCI::RDR();	// 受信データ読み出し
				}
			}

			//***************************************************************//
			// 文字列出力
			// param[in]	s : 出力文字列
			// return		なし
			//***************************************************************//
			void puts(const char* s) noexcept {
				if (s == nullptr) return;
				char ch;
				while((ch = *s++) != 0) {
					putch(ch);
				}
			}
	};

	// テンプレート関数、実態の定義
	template<class SCI, class RBF, class SBF, port_map::option PSEL, class HCTL>
		RBF sci_io<SCI, RBF, SBF, PSEL, HCTL>::recv_;

	template<class SCI, class RBF, class SBF, port_map::option PSEL, class HCTL>
		SBF sci_io<SCI, RBF, SBF, PSEL, HCTL>::send_;

	template<class SCI, class RBF, class SBF, port_map::option PSEL, class HCTL>
		bool sci_io<SCI, RBF, SBF, PSEL, HCTL>::soft_flow_;

	template<class SCI, class RBF, class SBF, port_map::option PSEL, class HCTL>
		volatile bool sci_io<SCI, RBF, SBF, PSEL, HCTL>::stop_;

	template<class SCI, class RBF, class SBF, port_map::option PSEL, class HCTL>
		volatile uint16_t sci_io<SCI, RBF, SBF, PSEL, HCTL>::errc_;
}