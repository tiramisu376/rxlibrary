# ==================================
#	RX6xx用スタートアップコード
# ==================================

	.extern __istack
	.extern __ustack
	.extern _interrupt_vectors
	.extern _init

	.text
	.global _start
	.type	_start, @function
_start:
# スタックポインタの設定
	mvtc	#__istack, isp	/* 割り込みスタックポインタ */
	mvtc	#__ustack, usp	/* ユーザースタックポインタ */

# 割り込みベクタの設定
	mvtc	#_interrupt_vectors, intb

# set FPSW
	mvtc	#100h, fpsw

# RAM領域のゼロクリア
	mov		#__datastart, r1
	mov		#0, r2
	mov		#__istack, r3
	sub		r1, r3
	sstr.b

# .bssセクションのクリア
	mov		#__bssstart, r1
	mov		#0, r2
	mov		#__bssend, r3
	sub		r1, r3
	sstr.b

# 初期値付き変数の初期化
	mov 	#__datastart, r1
	mov		#__romdatastart, r2
	mov		#__romdatacopysize, r3
	smovf

# Iレジスタを設定して、割り込みを許可する
# 割り込みレベル最低(全ての割り込みを許可する)
	mvfc	psw, r1
	or		#0x00010000, r1
	and		#0xf0ffffff, r1
	mvtc	r1, psw

# init()関数の実行
	bra		_init


	.global	_rx_run_preinit_array
	.type	_rx_run_preinit_array, @function
_rx_run_preinit_array:
	mov		#__preinit_array_start,r1
	mov		#__preinit_array_end,r2
	bra.a	_rx_run_inilist

	.global	_rx_run_init_array
	.type	_rx_run_init_array, @function
_rx_run_init_array:
	mov		#__init_array_start,r1
	mov		#__init_array_end,r2
	mov		#4, r3
	bra.a	_rx_run_inilist

	.global	_rx_run_fini_array
	.type	_rx_run_fini_array, @function
_rx_run_fini_array:
	mov		#__fini_array_start,r2
	mov		#__fini_array_end,r1
	mov		#-4, r3

# 初期化リスト
_rx_run_inilist:
next_inilist:
	cmp		r1,r2
	beq.b	done_inilist
	mov.l	[r1],r4
	cmp		#-1, r4
	beq.b	skip_inilist
	cmp	#0, r4
	beq.b	skip_inilist
	pushm	r1-r3
	jsr		r4
	popm	r1-r3

skip_inilist:
	add		r3,r1
	bra.b	next_inilist
done_inilist:
	rts

# プログラム終了ループ
# 通常はここへは飛んでこないはず
	.global _exit
_exit:
	wait
	bra		_exit
