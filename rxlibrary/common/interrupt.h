//=====================================================================//
// 割り込み処理関係
//=====================================================================//
#pragma once

#include <stdint.h>

#define INTERRUPT_FUNC __attribute__ ((interrupt))

#ifdef __cplusplus
extern "C" {
#endif

	//*****************************************************//
	// 割り込みベクタテーブルの初期化
	//*****************************************************//
	void init_interrupt(void);


	//*****************************************************//
	// 割り込み関数の設定
	// param[in]	interrupt_task : 割り込み関数
	// param[in]	vector		   : 割り込みベクタ番号
	// return		なし
	//*****************************************************//
	void set_interrupt_task(void (*interrupt_task)(void), uint32_t vector);

#ifdef __cplusplus
};
#endif