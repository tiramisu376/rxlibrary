// main関数実行前の初期化処理

int main(int argc, char**argv);

extern void rx_run_fini_array(void);
extern void rx_run_preinit_array(void);
extern void rx_run_init_array(void);

extern void init_interrupt(void);

// 初期化処理
void init(void) {
	// 割り込みベクタの初期化
	init_interrupt();

	// C++ static constractor
	rx_run_preinit_array();
	rx_run_init_array();
	rx_run_fini_array();

	// main関数の実行
	static int argc = 0;
	static char **argv = 0;
	int ret = main(argc, argv);

	// main関数が終了後のループ処理
	while(1);
}