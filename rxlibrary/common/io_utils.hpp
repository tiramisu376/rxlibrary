//===================================================
// I/Oアクセスユーティリティ
//===================================================
#pragma once

#include <cstdint>

namespace device {
	typedef uint32_t address_type;

	//*****************************************************//
	// 8ビット書き込み
	// param[in]	adr	 : 書き込みアドレス
	// param[in]	data : 書き込みデータ
	// return		なし
	//*****************************************************//
	inline void wr8_(address_type adr, uint8_t data) {
		*reinterpret_cast<volatile uint8_t *>(adr) = data;
 	}

	//*****************************************************//
 	// 8ビット読み込み
 	// param[in]	adr : 読み込みアドレス
	// return		アドレスの値
	//*****************************************************//
 	inline uint8_t rd8_(address_type adr) {
		 return *reinterpret_cast<volatile uint8_t *>(adr);
	}

	//*****************************************************//
	// 16ビット書き込み
	// param[in]	adr	 : 書き込みアドレス
	// param[in]	data : 書き込みデータ
	// return		なし
	//*****************************************************//
	inline void wr16_(address_type adr, uint16_t data) {
		*reinterpret_cast<volatile uint16_t *>(adr) = data;
 	}

	//*****************************************************//
 	// 16ビット読み込み
 	// param[in]	adr : 読み込みアドレス
	// return		アドレスの値
	//*****************************************************//
 	inline uint16_t rd16_(address_type adr) {
		 return *reinterpret_cast<volatile uint16_t *>(adr);
	}

	//*****************************************************//
	// 32ビット書き込み
	// param[in]	adr  : 書き込みアドレス
	// param[in]	data : 書き込みデータ
	// return		なし
	//*****************************************************//
	inline void wr32_(address_type adr, uint32_t data) {
		*reinterpret_cast<volatile uint32_t *>(adr) = data;
 	}

	//*****************************************************//
 	// 32ビット読み込み
 	// param[in]	adr : 読み込みアドレス
	// return		アドレスの値
	//*****************************************************//
 	inline uint16_t rd32_(address_type adr) {
		 return *reinterpret_cast<volatile uint32_t*>(adr);
	}


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 8ビット読み書きアクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct rw8_t {
		typedef uint8_t value_type;

		//*****************************************************//
		// アドレスの取得
		// param 	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 書き込み
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		static void write(value_type data) {
			wr8_(adr, data);
		}

		//*****************************************************//
		// 読み取り
		// param	なし
		// return	読み取り値
		//*****************************************************//
		static value_type read() {
			return rd8_(adr);
		}

		//*****************************************************//
		// = オペレーター
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		void operator = (value_type data) {
			write(data);
		}

		//*****************************************************//
		// () オペレーター
		// param	なし
		// return	読み取り値
		//*****************************************************//
		value_type operator () () {
			return read();
		}

		//*****************************************************//
		// |= オペレーター
		// param[in]	data : 論理和値
		// return		なし
		//*****************************************************//
		void operator |= (value_type data) {
			write(read() | data);
		}

		//*****************************************************//
		// &= オペレーター
		// param[in]	data : 論理積値
		// return		なし
		//*****************************************************//
		void operator &= (value_type data) {
			write(read() & data);
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 8ビット読み取り専用アクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct ro8_t {
		typedef uint8_t value_type;

		//*****************************************************//
		// アドレスの取得
		// param	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 読み取り
		// param	なし
		// return	読み取り値
		//*****************************************************//
		static value_type read() {
			return rd8_(adr);
		}

		//*****************************************************//
		// () オペレーター
		// param	なし
		// return	読み取り値
		//*****************************************************//
		value_type operator () () {
			return read();
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 8ビット書き込み専用アクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct wo8_t {
		typedef uint8_t value_type;

		//*****************************************************//
		// アドレスの取得
		// param	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 書き込み
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		static void write(value_type data) {
			wr8_(adr, data);
		}

		//*****************************************************//
		// = オペレーター
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		void operator = (value_type data) {
			write(data);
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 16ビット読み書きアクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct rw16_t {
		typedef uint16_t value_type;

		//*****************************************************//
		// アドレスの取得
		// param	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 書き込み
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		static void write(value_type data) {
			wr16_(adr, data);
		}

		//*****************************************************//
		// 読み取り
		// param	なし
		// return	読み取り値
		//*****************************************************//
		static value_type read() {
			return rd16_(adr);
		}

		//*****************************************************//
		// = オペレーター
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		void operator = (value_type data) {
			write(data);
		}

		//*****************************************************//
		// () オペレーター
		// param	なし
		// return	読み取り値
		//*****************************************************//
		value_type operator () () {
			return read();
		}

		//*****************************************************//
		// |= オペレーター
		// param[in]	data : 論理和値
		// return		なし
		//*****************************************************//
		void operator |= (value_type data) {
			write(read() | data);
		}

		//*****************************************************//
		// &= オペレーター
		// param[in]	data : 論理積値
		// return		なし
		//*****************************************************//
		void operator &= (value_type data) {
			write(read() & data);
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 16ビット読み取り専用アクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct ro16_t {
		typedef uint16_t value_type;

		//*****************************************************//
		// アドレスの取得
		// param	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 読み取り
		// param	なし
		// return	読み取り値
		//*****************************************************//
		static value_type read() {
			return rd16_(adr);
		}

		//*****************************************************//
		// () オペレーター
		// param	なし
		// return	読み取り値
		//*****************************************************//
		value_type operator () () {
			return read();
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 16ビット書き込み専用アクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct wo16_t {
		typedef uint16_t value_type;

		//*****************************************************//
		// アドレスの取得
		// param	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 書き込み
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		static void write(value_type data) {
			wr16_(adr, data);
		}

		//*****************************************************//
		// = オペレーター
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		void operator = (value_type data) {
			write(data);
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 32ビット読み書きアクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct rw32_t {
		typedef uint32_t value_type;

		//*****************************************************//
		// アドレスの取得
		// param	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 書き込み
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		static void write(value_type data) {
			wr32_(adr, data);
		}

		//*****************************************************//
		// 読み取り
		// param[in]	なし
		// return		読み取り値
		//*****************************************************//
		static value_type read() {
			return rd32_(adr);
		}

		//*****************************************************//
		// = オペレーター
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		void operator = (value_type data) {
			write(data);
		}

		//*****************************************************//
		// () オペレーター
		// param	なし
		// return	読み取り値
		//*****************************************************//
		value_type operator () () {
			return read();
		}

		//*****************************************************//
		// |= オペレーター
		// param[in]	data : 論理和値
		// return		なし
		//*****************************************************//
		void operator |= (value_type data) {
			write(read() | data);
		}

		//*****************************************************//
		// &= オペレーター
		// param[in]	data : 論理積値
		// return		なし
		//*****************************************************//
		void operator &= (value_type data) {
			write(read() & data);
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 32ビットテンポラリー読み書きアクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct trw32_t {
		typedef uint32_t value_type;
		static uint32_t tmp_;

		//*****************************************************//
		// アドレスの取得
		// param	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 書き込み(テンポラリー)
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		static void write(value_type data) {
			tmp_ = wr32_(adr, data);
		}

		//*****************************************************//
		// 読み取り(テンポラリー)
		// param	なし
		// return	読み取り値
		//*****************************************************//
		static value_type read() {
			return tmp_;
		}

		//*****************************************************//
		// = オペレーター
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		void operator = (value_type data) {
			write(data);
		}

		//*****************************************************//
		// () オペレーター
		// param	なし
		// return	読み取り値
		//*****************************************************//
		value_type operator () () {
			return read();
		}

		//*****************************************************//
		// |= オペレーター
		// param[in]	data : 論理和値
		// return		なし
		//*****************************************************//
		void operator |= (value_type data) {
			write(read() | data);
		}

		//*****************************************************//
		// &= オペレーター
		// param[in]	data : 論理積値
		// return		なし
		//*****************************************************//
		void operator &= (value_type data) {
			write(read() & data);
		}
	};
	template <address_type adr> uint32_t trw32_t<adr>::tmp_;


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 32ビット読み取り専用アクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct ro32_t {
		typedef uint32_t value_type;

		//*****************************************************//
		// アドレスの取得
		// param	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 読み取り
		// param	なし
		// return	読み取り値
		//*****************************************************//
		static value_type read() {
			return rd32_(adr);
		}

		//*****************************************************//
		// () オペレーター
		// param	なし
		// return	読み取り値
		//*****************************************************//
		value_type operator () () {
			return read();
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 32ビット書き込み専用アクセステンプレート
	// param[in]	adr : アドレス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <address_type adr>
	struct wo32_t {
		typedef uint32_t value_type;

		//*****************************************************//
		// アドレスの取得
		// param	なし
		// return	アドレス
		//*****************************************************//
		static address_type address() {
			return adr;
		}

		//*****************************************************//
		// 書き込み
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		static void write(value_type data) {
			wr32_(adr, data);
		}

		//*****************************************************//
		// = オペレーター
		// param[in]	data : 書き込み値
		// return		なし
		//*****************************************************//
		void operator = (value_type data) {
			write(data);
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// ビット位置定義
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	enum class bitpos : uint8_t {
		B0,			// ビット0
		B1,			// ビット1
		B2,			// ビット2
		B3,			// ビット3
		B4,			// ビット4
		B5,			// ビット5
		B6,			// ビット6
		B7,			// ビット7
		B8,			// ビット8
		B9,			// ビット9
		B10,		// ビット10
		B11,		// ビット11
		B12,		// ビット12
		B13,		// ビット13
		B14,		// ビット14
		B15,		// ビット15
		B16,		// ビット16
		B17,		// ビット17
		B18,		// ビット18
		B19,		// ビット19
		B20,		// ビット20
		B21,		// ビット21
		B22,		// ビット22
		B23,		// ビット23
		B24,		// ビット24
		B25,		// ビット25
		B26,		// ビット26
		B27,		// ビット27
		B28,		// ビット28
		B29,		// ビット29
		B30,		// ビット30
		B31,		// ビット31
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// ビット読み書き　アクセステンプレート
	// param[in]	T   : アクセスクラス
	// param[in]	pos : ビット位置
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class T, bitpos pos>
	struct bit_rw_t {
		static bool get() {
			return (T::read() >> static_cast<typename T::value_type>(pos)) & 1;
		}

		static void set(bool val) {
			if (val) {
				T::write(T::read() | (1 << static_cast<typename T::value_type>(pos)));
			} else {
				T::write(T::read() & ~(1 << static_cast<typename T::value_type>(pos)));
			}
		}

		static typename T::value_type bit(bool val = true) {
			return static_cast<typename T::value_type>(val) << static_cast<typename T::value_type>(pos);
		}

		void operator = (bool val) {
			set(val);
		}

		bool operator () () {
			return get();
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// ビット読み書き　アクセステンプレート
	// param[in]	T   : アクセスクラス
	// param[in]	pos : 初期ビット位置
	// param[in]	len : ビット幅
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class T, bitpos pos, uint8_t len>
	struct bits_rw_t {
		static typename T::value_type get() {
			return (T::read() >> static_cast<typename T::value_type>(pos)) & ((1 << len) - 1);
		}

		static void set(typename T::value_type val) {
			auto m = static_cast<typename T::value_type>(((1 << len) - 1) << static_cast<typename T::value_type>(pos));
			T::write((T::read() & ~m) | (static_cast<typename T::value_type>(val) << static_cast<typename T::value_type>(pos)));
		}

		static typename T::value_type bit(typename T::value_type val) {
			return (((1 << len) - 1) & val) << static_cast<typename T::value_type>(pos);
		}

		void operator = (typename T::value_type val) {
			set(val);
		}
		typename T::value_type operator () () {
			return get();
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// ビット読み取り専用　アクセステンプレート
	// param[in]	T   : アクセスクラス
	// param[in]	pos : 初期ビット位置
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class T, bitpos pos>
	struct bit_ro_t	{
		static bool get() {
			return (T::read() >> static_cast<typename T::value_type>(pos)) & 1;
		}

		static typename T::value_type bit() {
			return 1 << static_cast<typename T::value_type>(pos);
		}

		bool operator () () {
			return get();
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// ビット読み取り専用　アクセステンプレート
	// param[in]	T   : アクセスクラス
	// param[in]	pos : 初期ビット位置
	// param[in]	len : ビット幅
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class T, bitpos pos, uint8_t len>
	struct bits_ro_t {
		static typename T::value_type get() {
			return (T::read() >> static_cast<typename T::value_type>(pos)) & ((1 << len) - 1);
		}

		static typename T::value_type bit(typename T::value_type val) {
			return (((1 << len) - 1) & val) << static_cast<typename T::value_type>(pos);
		}

		typename T::value_type operator () () {
			return get();
		}
	};


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 標準バイト読み書き　アクセステンプレート
	// param[in]	T : アクセスクラス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class T>
	struct basic_rw_t : public T {
		using T::operator =;
		using T::operator ();
		using T::operator |=;
		using T::operator &=;

		typedef bit_rw_t<T, bitpos::B7> B7_;
		typedef bit_rw_t<T, bitpos::B6> B6_;
		typedef bit_rw_t<T, bitpos::B5> B5_;
		typedef bit_rw_t<T, bitpos::B4> B4_;
		typedef bit_rw_t<T, bitpos::B3> B3_;
		typedef bit_rw_t<T, bitpos::B2> B2_;
		typedef bit_rw_t<T, bitpos::B1> B1_;
		typedef bit_rw_t<T, bitpos::B0> B0_;
		static B7_ B7;	// B7アクセス
		static B6_ B6;	// B6アクセス
		static B5_ B5;	// B5アクセス
		static B4_ B4;	// B4アクセス
		static B3_ B3;	// B3アクセス
		static B2_ B2;	// B2アクセス
		static B1_ B1;	// B1アクセス
		static B0_ B0;	// B0アクセス
	};
	template <class T> typename basic_rw_t<T>::B7_ basic_rw_t<T>::B7;
	template <class T> typename basic_rw_t<T>::B6_ basic_rw_t<T>::B6;
	template <class T> typename basic_rw_t<T>::B5_ basic_rw_t<T>::B5;
	template <class T> typename basic_rw_t<T>::B4_ basic_rw_t<T>::B4;
	template <class T> typename basic_rw_t<T>::B3_ basic_rw_t<T>::B3;
	template <class T> typename basic_rw_t<T>::B2_ basic_rw_t<T>::B2;
	template <class T> typename basic_rw_t<T>::B1_ basic_rw_t<T>::B1;
	template <class T> typename basic_rw_t<T>::B0_ basic_rw_t<T>::B0;


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// 標準バイト読み取り専用　アクセステンプレート
	// param[in]	T : アクセスクラス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class T>
	struct basic_ro_t : public T {
		using T::operator ();

		typedef bit_rw_t<T, bitpos::B7> B7_;
		typedef bit_rw_t<T, bitpos::B6> B6_;
		typedef bit_rw_t<T, bitpos::B5> B5_;
		typedef bit_rw_t<T, bitpos::B4> B4_;
		typedef bit_rw_t<T, bitpos::B3> B3_;
		typedef bit_rw_t<T, bitpos::B2> B2_;
		typedef bit_rw_t<T, bitpos::B1> B1_;
		typedef bit_rw_t<T, bitpos::B0> B0_;
		static B7_ B7;	// B7アクセス
		static B6_ B6;	// B6アクセス
		static B5_ B5;	// B5アクセス
		static B4_ B4;	// B4アクセス
		static B3_ B3;	// B3アクセス
		static B2_ B2;	// B2アクセス
		static B1_ B1;	// B1アクセス
		static B0_ B0;	// B0アクセス
	};
	template <class T> typename basic_ro_t<T>::B7_ basic_ro_t<T>::B7;
	template <class T> typename basic_ro_t<T>::B6_ basic_ro_t<T>::B6;
	template <class T> typename basic_ro_t<T>::B5_ basic_ro_t<T>::B5;
	template <class T> typename basic_ro_t<T>::B4_ basic_ro_t<T>::B4;
	template <class T> typename basic_ro_t<T>::B3_ basic_ro_t<T>::B3;
	template <class T> typename basic_ro_t<T>::B2_ basic_ro_t<T>::B2;
	template <class T> typename basic_ro_t<T>::B1_ basic_ro_t<T>::B1;
	template <class T> typename basic_ro_t<T>::B0_ basic_ro_t<T>::B0;
}