//=====================================================//
// RX62Nグループ　CMT I/O制御
//=====================================================//
#pragma once

#include "rxlibrary.hpp"
#include "interrupt_utils.hpp"
#include "interrupt.h"

namespace device {
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// CMT I/O制御クラス
	// param[in]	CMT  : チャネルクラス
	// param[in]	TASK : タイマー動作クラス
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++//
	template <class CMT, class TASK = utils::null_task>
	class cmt_io {
		private:
			uint8_t  level_;
			uint32_t rate_;

			void sleep_() const { asm("nop"); }

			static TASK task_;

			static volatile uint32_t counter_;

			static INTERRUPT_FUNC void i_task_() {
				counter_++;
				task_();
			}

		public:
			//*****************************************************//
			// コンストラクター
			//*****************************************************//
			cmt_io() : level_(0), rate_(0) {}


			//*********************************************************************//
			// 開始(周波数を設定)
			// param[in]	freq  : タイマー周波数(Hz)
			// param[in]	level : 割り込みレベル(0ならポーリング)
			// param[in]	task  : 割り込み時に起動する関数
			//						※割り込み関数は属性「INTERRUPT_FUNC」を付加する
			// return		タイマー周波数が範囲を超えた場合「false」を返す
			//********************************************************************//
			bool startHz(uint32_t freq, uint8_t level = 0, void (*task)() = nullptr) {
				if (freq == 0) return false;

				uint32_t cmcor = CMT::PCLK / freq / 4;
				cmcor++;
				cmcor >>= 1;

				uint8_t cks = 0;
				while(cmcor > 65536) {
					cmcor >>= 2;
					cks++;
				}
				if (cks > 3 || cmcor == 0) {
					return false;
				}
				rate_ = freq;

				level_ = level;

				power_mgr::turn(CMT::PERIPHERAL);

				CMT::enable(false);

				CMT::CMCNT = 0;
				CMT::CMCOR = cmcor - 1;

				counter_ = 0;

				if (level_ > 0) {
					if (task != nullptr) {
						icu_mgr::set_interrupt(CMT::IVEC, task, level_);
					} else {
						icu_mgr::set_interrupt(CMT::IVEC, i_task_, level_);
					}
					CMT::CMCR = CMT::CMCR.CKS.bit(cks) | CMT::CMCR.CMIE.bit();
				} else {
					icu_mgr::set_interrupt(CMT::IVEC, nullptr, 0);
					CMT::CMCR = CMT::CMCR.CKS.bit(cks);
				}
				CMT::enable();

				return true;
			}


			//*********************************************************************//
			// 開始(時間を設定)
			// param[in]	ms	  : タイマー秒数(ms)
			// param[in]	level : 割り込みレベル(0ならポーリング)
			// param[in]	task  : 割り込み時に起動する関数
			//						※割り込み関数は属性「INTERRUPT_FUNC」を付加する
			// return		タイマー秒数が範囲を超えた場合「false」を返す
			//********************************************************************//
			bool startMs(uint32_t ms, uint8_t level = 0, void (*task)() = nullptr) {
				if (ms == 0) return false;

				uint32_t cmcor = CMT::PCLK / 4000 * ms;
				cmcor++;
				cmcor >>= 1;

				uint8_t cks = 0;
				while(cmcor > 65536) {
					cmcor >>= 2;
					cks++;
				}
				if (cks > 3 || cmcor == 0) {
					return false;
				}
				rate_ = ms;

				level_ = level;

				power_mgr::turn(CMT::PERIPHERAL);

				CMT::enable(false);

				CMT::CMCNT = 0;
				CMT::CMCOR = cmcor - 1;

				counter_ = 0;

				if (level_ > 0) {
					if (task != nullptr) {
						icu_mgr::set_interrupt(CMT::IVEC, task, level_);
					} else {
						icu_mgr::set_interrupt(CMT::IVEC, i_task_, level_);
					}
					CMT::CMCR = CMT::CMCR.CKS.bit(cks) | CMT::CMCR.CMIE.bit();
				} else {
					icu_mgr::set_interrupt(CMT::IVEC, nullptr, 0);
					CMT::CMCR = CMT::CMCR.CKS.bit(cks);
				}
				CMT::enable();

				return true;
			}


			//*********************************************************************//
			// 廃棄(割り込みを停止して、ユニットを停止)
			// param[in]	power : 電源を停止しない場合「true」
			// return		なし
			//********************************************************************//
			void destroy(bool power = false) {
				CMT::CMCR.CMIE = 0;
				CMT::enable(false);
				power_mgr::turn(CMT::PERIPHERAL, power);
			}


			//*********************************************************************//
			// 割り込みと同期
			// param	なし
			// return	なし
			//********************************************************************//
			void sync() const {
				if (level_ > 0) {
					volatile uint32_t cnt = counter_;
					while(cnt == counter_) sleep_();
				} else {
					auto ref = CMT::CMCNT();
					while(ref <= CMT::CMCNT()) sleep_();
					task_();
					counter_++;
				}
			}


			//*********************************************************************//
			// 割り込みカウンターの値を設定
			// param[in]	n : 割り込みカウンターの値
			// return		なし
			//********************************************************************//
			static void set_counter(uint32_t n) {
				counter_ = n;
			}


			//*********************************************************************//
			// 割り込みカウンターの値を取得
			// param[in]	なし
			// return		割り込みカウンターの値
			//********************************************************************//
			static uint32_t get_counter() {
				return counter_;
			}


			//*********************************************************************//
			// CMCNTレジスターの値を取得
			// param[in]	なし
			// return		CMCNTレジスターの値
			//********************************************************************//
			uint16_t get_cmt_count() const {
				return CMT::CMCNT();
			}


			//*********************************************************************//
			// CMCORレジスターの値を取得
			// param[in]	なし
			// return		CMCORレジスターの値
			//********************************************************************//
			uint16_t get_cmp_count() const {
				return CMT::CMCOR();
			}


			//*********************************************************************//
			// 周期を取得
			// param[in]	real : 「true」にした場合、内部で計算されたリアルな値
			// return		周期
			//********************************************************************//
			uint32_t get_rate(bool real = false) const {
				if (real) {
					uint32_t rate = CMT::PCLK / (static_cast<uint32_t>(CMT::CMCOR()) + 1);
					rate /= 8 << (CMT::CMCR.CKS() * 2);
					return rate;
				} else {
					return rate_;
				}
			}


			//*********************************************************************//
			// TASKクラスの参照
			// param	なし
			// return	TASKクラス
			//********************************************************************//
			static TASK& at_task() {
				return task_;
			}
	};

	template <class CMT, class TASK> volatile uint32_t cmt_io<CMT, TASK>::counter_ = 0;
	template <class CMT, class TASK> TASK cmt_io<CMT, TASK>::task_;
}